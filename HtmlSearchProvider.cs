﻿using HtmlEditorModule.Models;
using HtmlEditorModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;

namespace HtmlEditorModule
{
    public class HtmlSearchProvider : ISearchProvider
    {
        private Globals globals;
        public string ProviderName { get => "Html Search"; }
        public string ProviderDescription { get => "Searches HTML-Pages for given string"; }

        public string ProviderId { get; private set; }

        public bool ActionInAdditional2 => false;

        public async Task<ResultItem<List<clsOntologyItem>>> Search(string searchString, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };


               var controller = new HtmlEditorConnector(globals);

               var searchResult = await controller.GetHtmlSearchResult(searchString, messageOutput);

               result.ResultState = searchResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result = searchResult.Result.Select(res => new clsOntologyItem 
               { 
                   GUID = res.IdHtmlDocument, 
                   Name = res.NameHtmlDocument, 
                   GUID_Parent = Config.LocalData.Class_HTML_Document.GUID,
                   Additional1 = res.CodeSnipplet
               }).ToList();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<RankedWord>>> GetRankedFoundWordsByRefAndHtml(GetRankedFoundWordsByRefAndHtmlRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<List<RankedWord>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<RankedWord>()
                };

                var elasticAgent = new HtmlEditAgent_Elastic(globals);

                request.MessageOutput?.OutputInfo("Validate request...");
                var getOItemResult = await elasticAgent.GetOItem(request.IdRefItem, globals.Type_Object);
                result.ResultState = getOItemResult.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var htmlController = new HtmlEditorConnector(globals);

                request.MessageOutput?.OutputInfo("Get HTML-Document...");
                var getHtmlItemResult = await htmlController.GetHtmlDocument(getOItemResult.Result.GUID);
                result.ResultState = getHtmlItemResult.Result;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have HTML-Document.");

                var html = getHtmlItemResult.OAItemHtmlDocument.Val_String;
                var text = Converters.HtmlToPlainTextConverter.Convert(html);

                var informationRetrievalController = new InformationRetrievalController(globals);
                var rankedWords = await informationRetrievalController.GetRankedFoundWords(text);

                return result;
            });

            return taskResult;
        }

        public HtmlSearchProvider(Globals globals)
        {
            this.globals = globals;
            ProviderId = Guid.NewGuid().ToString();
        }
    }
}
