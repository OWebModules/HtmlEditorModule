﻿using HtmlEditorModule.Models;
using HtmlEditorModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Factories
{
    public class HtmlTemplateFactory
    {
        private Globals globals;
        public async Task<ResultItem<List<HTMLTemplateGridItem>>> GetHtmlTemplateGridItems(ResultHtmlTemplates resultElasticService, List<clsOntologyItem> transformedItems)
        {
            var taskResult = await Task.Run<ResultItem<List<HTMLTemplateGridItem>>>(() =>
           {
               var result = new ResultItem<List<HTMLTemplateGridItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<HTMLTemplateGridItem>()
               };

               result.Result = transformedItems.Select(trans => new HTMLTemplateGridItem { IdTemplate = trans.GUID, NameTemplate = trans.Name}).ToList();
            
               result.Result.AddRange(resultElasticService.TemplatesToHtmlDocuments.Select(htmlDocToTemp => new HTMLTemplateGridItem
               {
                   IdTemplate = htmlDocToTemp.ID_Object,
                   NameTemplate = htmlDocToTemp.Name_Other
               }));

               return result;
           });
            return taskResult;
        }

        public HtmlTemplateFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
