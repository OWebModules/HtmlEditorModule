﻿using HtmlEditorModule.Models;
using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace HtmlEditorModule.Factories
{
    public class LinkItemFactory
    {
        public async Task<ResultItem<List<LinkItem>>> CreateLinkItemList(List<MediaListItem> mediaListItems, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<LinkItem>>>(() =>
            {
                var result = new ResultItem<List<LinkItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = mediaListItems.Select(mediaListItem => new LinkItem
                {
                    NameLink = mediaListItem.IdMultimediaItem,
                    IdObject = mediaListItem.IdMultimediaItem,
                    NameObject = mediaListItem.NameMultimediaItem,
                    Url = mediaListItem.FileUrl,
                    IsAudio = mediaListItem.MediaType == MediaViewerModule.Primitives.MultimediaItemType.Audio,
                    IsImage = mediaListItem.MediaType == MediaViewerModule.Primitives.MultimediaItemType.Image,
                    IsVideo = mediaListItem.MediaType == MediaViewerModule.Primitives.MultimediaItemType.Video,
                    IsPDF = mediaListItem.MediaType == MediaViewerModule.Primitives.MultimediaItemType.PDF
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<LinkItem>>> CreateLinkItemList(string baseUrl, List<TypedTag> typedTags, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<LinkItem>>>(() =>
            {
                var result = new ResultItem<List<LinkItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = typedTags.Select(typedTag => new LinkItem
                {
                    NameLink = typedTag.IdTypedTag,
                    IdObject = typedTag.IdTag,
                    EncodedNameObject = typedTag.EncodedNameTag,
                    NameObject =  typedTag.NameTag,
                    Url = $"{baseUrl}?Object={typedTag.IdTag}"
                }).ToList();

                return result;
            });

            return taskResult;
        }
    }
}
