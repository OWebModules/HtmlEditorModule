﻿using HtmlEditorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Factories
{
    public class HtmlHistoryFactory
    {
        public async Task<ResultItem<List<HtmlHistoryItemWithRaw>>> GetHistoryEntries(clsOntologyItem oItemHtml, clsOntologyItem refItem, List<clsAppDocuments> documents, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<HtmlHistoryItemWithRaw>>>(() => {
                var result = new ResultItem<List<HtmlHistoryItemWithRaw>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = documents.Where(doc => doc.Dict.ContainsKey("datestamp") && doc.Dict.ContainsKey("Val_String")).Select(doc => new HtmlHistoryItemWithRaw
                {
                    HistoryItem = new HtmlHistoryItem
                    {
                        IdEsDoc = doc.Id,
                        IdRef = refItem?.GUID,
                        NameRef = refItem?.Name,
                        IdHtmlDoc = oItemHtml.GUID,
                        NameHtmlDoc = oItemHtml.Name,
                        HistoryStamp = (DateTime)doc.Dict["datestamp"]
                    },
                    EsDoc = doc
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<HtmlHistoryItem>>> GetHistoryEntries(HtmlHistoryEntriesRaw entries, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<HtmlHistoryItem>>>(() => {
                var result = new ResultItem<List<HtmlHistoryItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };



                result.Result = (from entry in entries.HtmlHistoryEntries.Where(doc => doc.Dict.ContainsKey("datestamp") && doc.Dict.ContainsKey("Val_String"))
                                join oItemHtml in entries.ObjectItems on entry.Dict["ID_Object"].ToString() equals oItemHtml.GUID
                                join refItem in entries.RefItems on oItemHtml.GUID equals refItem.ID_Object
                                select new HtmlHistoryItem
                                    {
                                        IdEsDoc = entry.Id,
                                        IdRef = refItem.ID_Other,
                                        NameRef = refItem.Name_Other,
                                        IdRefParent = refItem.ID_Parent_Other,
                                        TypeParent = refItem.Ontology,
                                        IdHtmlDoc = oItemHtml.GUID,
                                        NameHtmlDoc = oItemHtml.Name,
                                        HistoryStamp = (DateTime)entry.Dict["datestamp"]
                                    }).ToList();

                return result;
            });

            return taskResult;
        }
    }
}
