﻿using HtmlEditorModule.Models;
using HtmlEditorModule.Services;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Factories
{
    public class HtmlSearchResultFactory
    {
        private Globals globals;
        public async Task<ResultItem<List<HtmlSearchResultItem>>> CreateHtmlSearchResultList(ResultHtmlSearch searchResultRaw, string searchString)
        {
            var taskResult = await Task.Run<ResultItem<List<HtmlSearchResultItem>>>(() =>
           {
               var result = new ResultItem<List<HtmlSearchResultItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<HtmlSearchResultItem>()
               };


               result.Result = (from htmlCode in searchResultRaw.FoundHtmlCode
                                join htmlRef in searchResultRaw.FoundHtmlReferences on htmlCode.ID_Object equals htmlRef.ID_Object into htmlRefs
                                from htmlRef in htmlRefs.DefaultIfEmpty()
                                select new HtmlSearchResultItem
                                {
                                    IdHtmlDocument = htmlCode.ID_Object,
                                    NameHtmlDocument = htmlCode.Name_Object,
                                    IdDocumentClass = htmlCode.ID_Class,
                                    NameDocumentClass = htmlCode.Name_Class,
                                    IdReference = htmlRef?.ID_Other,
                                    NameReference = htmlRef?.Name_Other,
                                    IdParentReference = htmlRef?.ID_Parent_Other,
                                    NameParentReference = htmlRef?.Name_Parent_Other,
                                    CodeSnipplet = htmlCode.Val_String
                                }).ToList();

               var offset = 50;
               foreach (var searchResult in result.Result)
               {
                   var lastIndexOfCodeSnipplet = searchResult.CodeSnipplet.Length-1;
                   var indexStart = searchResult.CodeSnipplet.ToLower().IndexOf(searchString.ToLower());
                   var indexEnd = searchString.Length;
                   var firstIndex = indexStart >= offset ? indexStart - offset : 0;
                   var lastIndex = indexEnd + offset <= lastIndexOfCodeSnipplet ? indexEnd + offset : lastIndexOfCodeSnipplet;
                   searchResult.CodeSnipplet = searchResult.CodeSnipplet.Substring(firstIndex, lastIndex);
               }

               return result;
           });

            return taskResult;
        }

        public HtmlSearchResultFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
