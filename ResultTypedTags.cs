﻿using OntologyClasses.BaseClasses;
using System.Collections.Generic;
using TypedTaggingModule.Models;

namespace HtmlEditorModule
{
    public class ResultTypedTags
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem TaggingSource { get; set; }
        public List<clsOntologyItem> Tags { get; set; }
        public List<TypedTag> TypedTags { get; set; }
    }
}
