﻿using OntologyClasses.BaseClasses;

namespace HtmlEditorModule
{
    public class ResultClassPath
    {
        public clsOntologyItem Result { get; set; }
        public string Path { get; set; }
    }
}
