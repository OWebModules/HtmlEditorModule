﻿using OntologyClasses.BaseClasses;
using System.Collections.Generic;

namespace HtmlEditorModule
{
    public class ConnectorResultHtmlDocument
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OItemRef { get; set; }
        public clsOntologyItem OItemHtmlDocument { get; set; }
        public clsObjectAtt OAItemHtmlDocument { get; set; }
        public List<clsObjectAtt> OAItemStyles { get; set; } = new List<clsObjectAtt>();
    }
}
