﻿using HtmlAgilityPack;
using HtmlEditorModule.Factories;
using HtmlEditorModule.Models;
using HtmlEditorModule.Services;
using MediaStore_Module;
using MediaStore_Module.Models;
using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ReportModule;
using ReportModule.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using TypedTaggingModule.Models;

namespace HtmlEditorModule
{
    public class ExportHtmlDocController : AppController
    {
        public static clsOntologyItem DocTypeTableRow { get { return Export.Config.LocalData.Object_Table_Row; } }
        public static clsOntologyItem DocTypeTable { get { return Export.Config.LocalData.Object_Table; } }
        public static clsOntologyItem DocTypeImages { get { return Export.Config.LocalData.Object_Images; } }
        public static clsOntologyItem DocTypeParagraph { get { return Export.Config.LocalData.Object_Paragraph; } }
        public static clsOntologyItem DocTypeContent { get { return Export.Config.LocalData.Object_Content; } }
        public static clsOntologyItem DocTypeTitle { get { return Export.Config.LocalData.Object_Title; } }
        public static clsOntologyItem DocTypeBody { get { return Export.Config.LocalData.Object_Content; } }
        public static clsOntologyItem DocTypeDocumentInit { get { return Export.Config.LocalData.Object_Document_Init_Final; } }
        public static clsOntologyItem DocTypeHead { get { return Export.Config.LocalData.Object_Document_Head_Init_Final; } }
        public static clsOntologyItem DocTypeBold { get { return Export.Config.LocalData.Object_Bold; } }
        public static clsOntologyItem DocTypeHeading { get { return Export.Config.LocalData.Object_Heading; } }
        public static clsOntologyItem AttributeHEIGHT { get { return Export.Config.LocalData.Object_height; } }
        public static clsOntologyItem AttributeWIDHT { get { return Export.Config.LocalData.Object_width; } }
        public static clsOntologyItem AttributeSRC { get { return Export.Config.LocalData.Object_src; } }
        public static clsOntologyItem AttributeBorder { get { return Export.Config.LocalData.Object_border; } }
        public static clsOntologyItem DocTypePDFsMedia { get { return Export.Config.LocalData.Object_PDFs_Media; } }
        public static clsOntologyItem DocTypeTableCol { get { return Export.Config.LocalData.Object_Table_Col; } }

        public static clsObjectAtt HtmlIntro { get { return Export.Config.LocalData.ObjAtt_HTML_Intro_XML_Text; } }

        public string EncodeHTML(string input)
        {
            input = HttpUtility.HtmlEncode(input);

            return input;
        }

        public clsOntologyItem DocumenTypeBold
        {
            get
            {
                return Export.Config.LocalData.Object_Bold;
            }
        }

        public async Task<clsOntologyItem> ExportAnonymousHtml(ExportAnonymousHtmlRequest request, bool isAspNetProject)
        {

            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var connector = new HtmlEditorConnector(Globals);
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var nameTransformProviders = NameTransformManager.GetNameTransformProviders(Globals).Where(provider => provider.IsReferenceCompatible).ToList();

                var result = Globals.LState_Success.Clone();

                var getMediaStoreHostResult = await mediaStoreConnector.GetMediaServiceHost();

                result = getMediaStoreHostResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var serviceAgent = new HtmlEditAgent_Elastic(Globals);

                var serviceResult = await serviceAgent.GetExportAnonymousHtmlModel(request);
                result = serviceResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Export Html-Pack...");
                result = ExportHtmlPack(request, serviceResult.Result, getMediaStoreHostResult.Result, request.MessageOutput);


                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Exported Html-Pack.");
                var keywords = new List<string>();

                request.MessageOutput?.OutputInfo("Get reports-config for report-export...");
                var exportReportConfigs = await serviceAgent.GetExportReportModel(serviceResult.Result.ExportReportItems);
                result = exportReportConfigs.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have reports-config for report-export.");

                var reportController = new ReportController(Globals, isAspNetProject);


                try
                {

                    var relationConfig = new clsRelationConfig(Globals);
                    var saveStats = new List<clsOntologyItem>();
                    var saveAttributes = new List<clsObjectAtt>();
                    var saveRels = new List<clsObjectRel>();

                    foreach (var config in serviceResult.Result.Configs)
                    {
                        var url = serviceResult.Result.Urls.FirstOrDefault(urlItm => urlItm.ID_Object == config.GUID);
                        var path = serviceResult.Result.Paths.FirstOrDefault(pathItm => pathItm.ID_Object == config.GUID);
                        var reports = (from repCfg in exportReportConfigs.Result.ConfigsToExportReportsConfig.Where(repCfg => repCfg.ID_Object == config.GUID)
                                       join rep in exportReportConfigs.Result.Reports on repCfg.ID_Other equals rep.ID_Object
                                       select rep).ToList();

                        if (reports.Any())
                        {
                            request.MessageOutput?.OutputInfo("Export Reports...");
                            var reportOItems = reports.Select(rep => new clsOntologyItem
                            {
                                GUID = rep.ID_Other,
                                Name = rep.Name_Other,
                                GUID_Parent = rep.ID_Parent_Other,
                                GUID_Related = rep.ID_Object,
                                Type = rep.Ontology
                            }).ToList();

                            foreach (var reportOItem in reportOItems)
                            {
                                request.MessageOutput?.OutputInfo($"Report: {reportOItem.Name}");
                                var getReportResult = await reportController.GetReport(reportOItem);

                                var fileFields = exportReportConfigs.Result.FilesFields.Where(file => file.ID_Object == reportOItem.GUID_Related).ToList();
                                var reportFilter = exportReportConfigs.Result.ReportFilters.Where(filter => filter.ID_Object == reportOItem.GUID_Related).Select(rel => new clsOntologyItem
                                {
                                    GUID = rel.ID_Other,
                                    Name = rel.Name_Other,
                                    GUID_Parent = rel.ID_Parent_Other,
                                    Type = rel.Ontology
                                }).FirstOrDefault();
                                var reportSort = exportReportConfigs.Result.ReportSorts.Where(sort => sort.ID_Object == reportOItem.GUID_Related).Select(rel => new clsOntologyItem
                                {
                                    GUID = rel.ID_Other,
                                    Name = rel.Name_Other,
                                    GUID_Parent = rel.ID_Parent_Other,
                                    Type = rel.Ontology
                                }).FirstOrDefault();

                                var jsonTableTemplate = exportReportConfigs.Result.JsonTableTemplates.FirstOrDefault(table => table.ID_Object == reportOItem.GUID_Related);
                                var jsonRowTemplate = exportReportConfigs.Result.JsonRowTemplates.FirstOrDefault(table => table.ID_Object == reportOItem.GUID_Related);

                                result = getReportResult.ResultState;

                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.Additional1);
                                    return result;
                                }

                                var getReportFieldsResult = await reportController.GetReportFields(reportOItem);

                                var whereClause = "";
                                var sortClause = "";

                                if (reportFilter != null)
                                {
                                    var filterRequest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByFilterOItems) { FilterOItems = new List<clsOntologyItem> { reportFilter } };
                                    var getReportFilterResult = await reportController.GetFilterItems(filterRequest);
                                    result = getReportFilterResult.ResultState;
                                    if (result.GUID == Globals.LState_Error.GUID)
                                    {
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }

                                    var whereClauseObj = getReportFilterResult.Result.FirstOrDefault();
                                    whereClause = whereClauseObj?.Value.Val_String;
                                }
                                
                                if (reportSort != null)
                                {
                                    var sortRequest = new GetSortItemsRequest(new List<clsOntologyItem> { reportSort });
                                    var getSortResult = await reportController.GetSortItems(sortRequest);
                                    result = getSortResult.ResultState;
                                    if (result.GUID == Globals.LState_Error.GUID)
                                    {
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }
                                    result = getReportFieldsResult.ResultState;

                                    if (result.GUID == Globals.LState_Error.GUID)
                                    {
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }

                                    var sortClauseObj = getSortResult.Result.FirstOrDefault();
                                    sortClause = sortClauseObj?.Value.Val_String;
                                }


                                if (getReportResult.Result != null)
                                {
                                    var data = await reportController.LoadData(getReportResult.Result.Report, getReportFieldsResult.Result, whereClause, sortClause);

                                    var rootPath = Path.Combine(path.Name_Other, "Ressources");
                                    var jsonFilePath = Path.Combine(path.Name_Other, $"{reportOItem.GUID_Related}.json");

                                    var lenTemplate = jsonTableTemplate.Val_String.Length;
                                    var rowsPos = jsonTableTemplate.Val_String.IndexOf("@ROWS@");
                                    var afterRowsPos = rowsPos + "@ROWS@".Length;
                                    var restLenght = lenTemplate - afterRowsPos;
                                    var header = jsonTableTemplate.Val_String.Substring(0, rowsPos);
                                    var footer = jsonTableTemplate.Val_String.Substring(afterRowsPos, restLenght);

                                    var jsonBuilder = new StringBuilder();
                                    using (var streamWriter = new StreamWriter(jsonFilePath))
                                    {
                                        streamWriter.WriteLine(header);
                                        for (int i = 0; i < data.Result.Count; i++)
                                        {
                                            var row = data.Result[i];
                                            var jsonRow = Newtonsoft.Json.JsonConvert.SerializeObject(row);
                                            foreach (var reportField in getReportFieldsResult.Result)
                                            {
                                                
                                                
                                                if (fileFields.Any())
                                                {
                                                    var fileField = fileFields.FirstOrDefault(field => field.Name_Other == reportField.NameReportField);
                                                    if (fileField != null)
                                                    {
                                                        var idFile = row[fileField.Name_Other].ToString();

                                                        var exportResult = ExportFile(rootPath, idFile, getMediaStoreHostResult.Result);
                                                        if (exportResult.GUID == Globals.LState_Error.GUID)
                                                        {
                                                            request.MessageOutput?.OutputError(result.Additional1);
                                                        }
                                                    }
                                                }
                                            }
                                            if (i < data.Result.Count-1)
                                            {
                                                jsonRow += ",";
                                            }

                                            streamWriter.WriteLine(jsonRow);
                                        }
                                        
                                        streamWriter.WriteLine(footer);
                                    }

                                    

                                }



                            }

                            request.MessageOutput?.OutputInfo("Exported Reports.");
                        }

                        var exportLastEditObj = serviceResult.Result.ExportLastEdit.FirstOrDefault(rel => rel.ID_Object == config.GUID);
                        var exportLastEdit = exportLastEditObj != null ? exportLastEditObj.Val_Bool.Value : false;
                        var exportDateTimeStampStart = DateTime.MinValue;
                        var exportDateTimeStampEnd = DateTime.Now;
                        if (exportLastEdit)
                        {
                            var exportStamp = (from stat1 in serviceResult.Result.StatsToConfigs.Where(statRel => statRel.ID_Other == config.GUID)
                                               join stamp in serviceResult.Result.StatsDateTime on stat1.ID_Object equals stamp.ID_Object
                                               select stamp).FirstOrDefault();

                            if (exportStamp != null)
                            {
                                exportDateTimeStampStart = exportStamp.Val_Datetime.Value.AddSeconds(1);
                            }
                        }

                        request.MessageOutput?.OutputInfo("Create Index...");
                        var indexItem = serviceResult.Result.IndexCommandLineRuns.FirstOrDefault(ix => ix.ID_Object == config.GUID);
                        var commandLineRunConnector = new CommandLineRunModule.CommandLineRunController(Globals);
                        var commandLineRunTask = await commandLineRunConnector.GetCommandLineRun(new clsOntologyItem
                        {
                            GUID = indexItem.ID_Other,
                            Name = indexItem.Name_Other,
                            GUID_Parent = indexItem.ID_Parent_Other,
                            Type = indexItem.Ontology
                        });

                        result = commandLineRunTask.ResultState;
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var htmlCode = commandLineRunTask.Result.CodeParsed;

                        var docHtmlCmdrls = serviceResult.Result.ForDocsCommandLineRuns.Where(cmdrl => cmdrl.ID_Object == config.GUID).ToList();
                        var errorDocsHtml = false;
                        var searchEntries = new List<SearchEntry>();
                        var htmlCodeDocsWithoutReferences = new List<ReferencedHtmlCode>();
                        var htmlCodeDocsWithReferences = new List<ReferencedHtmlCode>();
                        foreach (var docHtml in docHtmlCmdrls)
                        {
                            if (errorDocsHtml) break;
                            var docHtmlsTasks = await commandLineRunConnector.GetCommandLineRun(new clsOntologyItem
                            {
                                GUID = docHtml.ID_Other,
                                Name = docHtml.Name_Other,
                                GUID_Parent = docHtml.ID_Parent_Other,
                                Type = docHtml.Ontology
                            });

                            if (docHtmlsTasks.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                errorDocsHtml = true;
                                break;
                            }

                            if (docHtmlsTasks.Result.CmdrlToReferences.Any())
                            {
                                htmlCodeDocsWithReferences.Add(new ReferencedHtmlCode
                                {
                                    HtmlCode = docHtmlsTasks.Result.CodeParsed,
                                    ReferenceIds = docHtmlsTasks.Result.CmdrlToReferences.Select(refItem => refItem.ID_Other).ToList()
                                });
                            }
                            else
                            {
                                htmlCodeDocsWithoutReferences.Add(new ReferencedHtmlCode
                                {
                                    HtmlCode = docHtmlsTasks.Result.CodeParsed,
                                    ReferenceIds = docHtmlsTasks.Result.CmdrlToReferences.Select(refItem => refItem.ID_Other).ToList()
                                });
                            }
                        }
                        
                        if (errorDocsHtml)
                        {
                            result = Globals.LState_Error.Clone();

                            result.Additional1 = "Errors while getting the Html-Codes for the Documents!";
                            return result;
                        }
                        
                        result = await ExportIndex(htmlCode, path, keywords, "Index");
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        request.MessageOutput?.OutputInfo("Created Index.");

                        request.MessageOutput?.OutputInfo("Add Search-Entries...");
                        var outputPath = Path.Combine(path.Name_Other, "Index.html");
                        result = AddSearchEntries("Index", "Index", outputPath, "Index.html", searchEntries);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        request.MessageOutput?.OutputInfo("Added Search-Entries.");

                        request.MessageOutput?.OutputInfo("Create Anonymous...");
                        var filePath = System.IO.Path.Combine(path.Name_Other, "anonymoushtmldocuments.json");

                        var allowedItems = serviceResult.Result.AnonymousItems.Where(ano => ano.ConfigItem == config).ToList();

                        System.IO.File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(allowedItems, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8);
                        request.MessageOutput?.OutputInfo("Created Anonymous");

                        var listTypedTags = new List<TypedTag>();
                        var count = allowedItems.Count;
                        var done = 0;
                        request.MessageOutput?.OutputInfo($"Export {count} documents...");
                        foreach (var allowedItem in allowedItems)
                        {
                            request.MessageOutput?.OutputInfo($"Export Document: {allowedItem.AnonymousAllowed.GUID}");
                            clsObjectAtt oAItemHtmlDocument = null;
                            var connectorResult = await connector.GetHtmlDocument(allowedItem.AnonymousAllowed.GUID);
                            if (connectorResult.Result.GUID == connector.Globals.LState_Success.GUID)
                            {
                                oAItemHtmlDocument = connectorResult.OAItemHtmlDocument;
                            }

                            if (exportLastEdit)
                            {
                                request.MessageOutput?.OutputInfo($"Check last edit...");
                                var getHtmlHistoryEntriesResult = await serviceAgent.GetHtmlHistoryEntries(allowedItem.AnonymousAllowed);
                                result = getHtmlHistoryEntriesResult.ResultState;
                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.Additional1);
                                    return result;
                                }

                                var factory = new HtmlHistoryFactory();

                                var factoryResult = await factory.GetHistoryEntries(allowedItem.AnonymousAllowed, null, getHtmlHistoryEntriesResult.Result, Globals);

                                result = factoryResult.ResultState;

                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.Additional1);
                                    return result;
                                }
                                var lastEdit = factoryResult.Result.OrderByDescending(entry => entry.HistoryItem.HistoryStamp).FirstOrDefault();
                                if (!factoryResult.Result.Any(entry => entry.HistoryItem.HistoryStamp >= exportDateTimeStampStart))
                                {
                                    request.MessageOutput?.OutputInfo("Add Search-Entries...");
                                    outputPath = Path.Combine(path.Name_Other, $"{oAItemHtmlDocument.ID_Object}.html");
                                    result = AddSearchEntries(allowedItem.AnonymousAllowed.GUID, allowedItem.AnonymousAllowed.Name, outputPath, $"{oAItemHtmlDocument.ID_Object}_C.html", searchEntries);
                                    if (result.GUID == Globals.LState_Error.GUID)
                                    {
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }
                                    request.MessageOutput?.OutputInfo("Added Search-Entries.");

                                    var exportStamp = lastEdit != null ? lastEdit.HistoryItem.HistoryStamp.ToString("yyyy-MM-dd HH:mm:ss") : " - ";
                                    request.MessageOutput?.OutputInfo($"No export, last edit: {exportStamp}");
                                    continue;
                                }
                                else
                                {
                                    var exportStamp = lastEdit != null ? lastEdit.HistoryItem.HistoryStamp.ToString("yyyy-MM-dd HH:mm:ss") : " - ";
                                    request.MessageOutput?.OutputInfo($"Export, last edit: {exportStamp}");
                                }
                            }

                            request.MessageOutput?.OutputInfo($"Create Html...");
                            var regexChange = new Regex("href=\"HtmlEditorJQ.html.Class=" + connector.Globals.RegexGuid + "(.|.{5})Object=" + connector.Globals.RegexGuid + "\"");
                            var regexObject = new Regex("(O|o)bject=" + connector.Globals.RegexGuid);
                            var regexObjectId = new Regex(connector.Globals.RegexGuid);
                            var contentBuilder = new StringBuilder();
                            if (oAItemHtmlDocument != null && oAItemHtmlDocument.Val_String != null)
                            {
                                contentBuilder.AppendLine("<div id='title" + oAItemHtmlDocument.ID_Object + "' style='visibility:hidden'>" + oAItemHtmlDocument.Name_Object + "</div>");
                                var objectId = "";
                                var text = oAItemHtmlDocument.Val_String;
                                var matches = regexChange.Matches(text);
                                if (matches.Count > 0)
                                {
                                    var start = 0;
                                    matches.Cast<Match>().ToList().ForEach(matchItem =>
                                    {
                                        var index = matchItem.Index;
                                        var length = matchItem.Length;
                                        matchItem = regexObject.Match(matchItem.Value);
                                        if (matchItem.Success)
                                        {
                                            var objectString = matchItem.Value;
                                            matchItem = regexObjectId.Match(objectString);
                                            if (matchItem.Success)
                                            {
                                                objectId = matchItem.Value;
                                            }

                                            contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start, index - start));
                                            contentBuilder.Append("href=\"/iDoku/iDoku/Index/" + objectId + "\"");
                                            start = index + length;

                                        }

                                    });
                                    if (start < text.Length)
                                    {
                                        contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start));
                                    }
                                }
                                else
                                {
                                    contentBuilder.Append(oAItemHtmlDocument.Val_String);
                                }

                                request.MessageOutput?.OutputInfo($"Created Html");


                                result = ExportMedia(contentBuilder, path, url, getMediaStoreHostResult.Result);
                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.Additional1);
                                    return result;
                                }

                                filePath = System.IO.Path.Combine(path.Name_Other, oAItemHtmlDocument.ID_Object + ".html");
                                System.IO.File.WriteAllText(filePath, contentBuilder.ToString());
                                request.MessageOutput?.OutputInfo($"Exported Html");

                                var document = new HtmlDocument();
                                document.LoadHtml(contentBuilder.ToString());
                                var additional = document.DocumentNode.InnerText;
                                if (additional.Length > 156)
                                {
                                    additional = $"{additional.Substring(0, 157)}...";
                                }

                                allowedItem.ConfigItem.Additional1 = additional;

                                request.MessageOutput?.OutputInfo("Create content index...");

                                var htmlCodeDoc = htmlCode;
                                var docForHtml = htmlCodeDocsWithReferences.FirstOrDefault(doc => doc.ReferenceIds.Contains(oAItemHtmlDocument.ID_Object));
                                if (docForHtml == null)
                                {
                                    docForHtml = htmlCodeDocsWithoutReferences.FirstOrDefault();
                                }
                                if (docForHtml != null)
                                {
                                    htmlCodeDoc = docForHtml.HtmlCode;
                                }

                                result = await ExportIndex(htmlCodeDoc, path, new List<string>(), $"{oAItemHtmlDocument.ID_Object}_C", oAItemHtmlDocument.ID_Object, allowedItem.ConfigItem.Additional1, allowedItem.AnonymousAllowed.Name);
                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    request.MessageOutput?.OutputError(result.Additional1);
                                    return result;
                                }

                                request.MessageOutput?.OutputInfo("Created content index.");


                                var metaJsonResult = await WriteMetaJson(allowedItem,
                                    serviceResult.Result.AnonymousItems,
                                    url, path,
                                    connector,
                                    serviceResult.Result,
                                    request.MessageOutput,
                                    nameTransformProviders);

                                keywords.AddRange(metaJsonResult.Result.Select(typedTag => HttpUtility.HtmlEncode(typedTag.NameTag)));


                                listTypedTags.AddRange(metaJsonResult.Result);
                                result = metaJsonResult.ResultState;
                                result = await WriteMetaTree(allowedItem.AnonymousAllowed.GUID, connector, url, path, serviceResult.Result, request.MessageOutput);

                                request.MessageOutput?.OutputInfo($"-----");
                            }

                            done++;

                            if (done % 10 == 0)
                            {
                                request.MessageOutput?.OutputInfo($"Exported {done} documents");
                            }
                        }

                        if (searchEntries.Any())
                        {
                            filePath = System.IO.Path.Combine(path.Name_Other, "searchentries.json");
                            File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(searchEntries, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8);
                        }

                        var dateState = DateTime.Now;
                        var stat = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = dateState.ToString("yyyy-MM-dd HH:mm:ss"),
                            GUID_Parent = Config.LocalData.Class_Export_Stat__Anonymous_.GUID,
                            Type = Globals.Type_Object
                        };

                        saveStats.Add(stat);

                        var nextOrderIdResult = await serviceAgent.GetNextOrderId(config);
                        var statToExport = relationConfig.Rel_ObjectRelation(stat, config, Config.LocalData.RelationType_belongs_to);
                        statToExport.OrderID = nextOrderIdResult.Result;
                        saveRels.Add(statToExport);

                        saveRels.AddRange(allowedItems.Select(ano => relationConfig.Rel_ObjectRelation(stat, ano.AnonymousAllowed, Config.LocalData.RelationType_belonging_Document)));

                        var attribute = relationConfig.Rel_ObjectAttribute(stat, Config.LocalData.AttributeType_Datetimestamp__Create_, dateState);
                        saveAttributes.Add(attribute);

                        var saveResult = await serviceAgent.SaveExportStat(saveStats, saveRels, saveAttributes);
                        result = saveResult;
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        request.MessageOutput?.OutputInfo($"Exported {count} documents");




                    }


                    return result;
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> ExportReportJsons(clsOntologyItem configItem, clsObjectRel pathRel, ExportReportModel exportModel)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = Globals.LState_Success.Clone();



                return result;
            });

            return taskResult;
        }

        public string GetHtmlIntro()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Export.Config.LocalData.ObjAtt_HTML_Intro_XML_Text.Val_String);
            var element = xmlDoc.GetElementsByTagName("data").Cast<XmlElement>().FirstOrDefault();
            if (element == null) return string.Empty;

            return element.InnerText;
        }

        public async Task<ResultItem<string>> GetHtmlTag(GetHtmlTagRequest request)
        {

            var taskResult = await Task.Run<ResultItem<string>>(async () =>
            {
                var result = new ResultItem<string>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                result.Result = "";
                var htmlEndInit = "";
                var htmlEnd = "";

                var start = Export.Config.LocalData.Object_lt.Name;

                if (!string.IsNullOrEmpty(start))
                {
                    result.Result = start;
                    if (request.Finalize)
                    {
                        htmlEndInit = Export.Config.LocalData.Object_slash.Name;

                        if (!string.IsNullOrEmpty(htmlEndInit))
                        {
                            result.Result += htmlEndInit;
                        }
                        else
                        {
                            result.Result = null;
                        }
                    }

                    if (!string.IsNullOrEmpty(result.Result))
                    {
                        var serviceAgent = new HtmlEditAgent_Elastic(Globals);
                        long? orderId = request.OrderId != null ? request.OrderId.Value : (long?)null;
                        var tagItem = await serviceAgent.GetHtmlTag(request.DocumentType, orderId);

                        if (tagItem != null)
                        {
                            result.Result += tagItem.Result.Name;

                            if (!request.Finalize)
                            {
                                if (request.Attributes != null && request.Attributes.Any())
                                {
                                    result.Result = request.Attributes.Aggregate(result.Result, (current, attribute) => current + (" " + attribute.Key + "=" + "\"" + attribute.Value + "\""));
                                }

                            }

                            htmlEnd = Export.Config.LocalData.Object_gt.Name;

                            if (!string.IsNullOrEmpty(htmlEnd))
                            {
                                result.Result += htmlEnd;
                            }
                            else
                            {
                                result.Result = null;
                            }
                        }
                        else
                        {
                            result.Result = null;
                        }
                    }
                }
                else
                {
                    result.Result = null;
                }

                return result;
            });

            return taskResult;

        }

        private async Task<clsOntologyItem> ExportIndex(string htmlCode,
                                                        clsObjectRel configToPath,
                                                        List<string> keywords,
                                                        string fileName,
                                                        string idDoc = null,
                                                        string description = null,
                                                        string header = null)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                htmlCode = htmlCode.Replace("@KEY_WORDS@", string.Join(", ", keywords));
                if (!string.IsNullOrEmpty(description))
                {
                    description = HttpUtility.HtmlEncode(description);
                    description = description.Replace("\n", "<br>");
                    description = description.Replace("\r", "");
                    var regex = new Regex("<meta name=\"description\" content=\".*\">");
                    var descriptionMeta = $"<meta name=\"description\" content=\"{description}\">";
                    htmlCode = regex.Replace(htmlCode, descriptionMeta);
                    regex = new Regex("<meta property=\"og:description\" content=\".*\">");
                    var descriptionOg = $"<meta property=\"og:description\" content=\"{description}\">";
                    htmlCode = regex.Replace(htmlCode, descriptionOg);
                }

                if (!string.IsNullOrEmpty(idDoc))
                {
                    var regex = new Regex("idDoc = \".*\";");
                    idDoc = $"idDoc = \"{idDoc}\";";
                    htmlCode = regex.Replace(htmlCode, idDoc);
                }

                if (!string.IsNullOrEmpty(header))
                {
                    var regexTitle = new Regex("<title>.*</title>");
                    var regesOgTitle = new Regex("<meta property=\"og:title\" content=\".*\">");
                    var title = $"<title>{HttpUtility.HtmlEncode(header)}</title>";
                    var ogTitle = $"<meta property=\"og:title\" content=\"{HttpUtility.HtmlEncode(header)}\">";
                    var regexHeader = new Regex("<div id=\"headerArea\"></div>");
                    header = $"<div id=\"headerArea\"><h1>{HttpUtility.HtmlEncode(header)}</h1></div>";
                    htmlCode = regexTitle.Replace(htmlCode, title);
                    htmlCode = regesOgTitle.Replace(htmlCode, ogTitle);
                    htmlCode = regexHeader.Replace(htmlCode, header);
                }

                var outputPath = Path.Combine(configToPath.Name_Other, $"{fileName}.html");

                try
                {
                    File.WriteAllText(outputPath, htmlCode);
                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = $"Error while getting the Index: {ex.Message}";

                }

                return result;
            });

            return taskResult;
        }

        private clsOntologyItem AddSearchEntries(string id, string title, string filePath, string fileName, List<SearchEntry> searchEntries)
        {
            var result = Globals.LState_Success.Clone();

            try
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.Load(filePath);
                var sbText = new StringBuilder();
                foreach (HtmlNode node in htmlDoc.DocumentNode.SelectNodes("//text()"))
                {
                    sbText.AppendLine(node.InnerText);
                }

                if (sbText.Length > 0)
                {
                    var searchEntry = new SearchEntry(id, sbText.ToString(), title, fileName);
                    searchEntries.Add(searchEntry);
                }
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
            }

            return result;
        }
        private clsOntologyItem ExportHtmlPack(ExportAnonymousHtmlRequest request, GetModelRawResult modelResult, MediaServiceHost serviceHost, IMessageOutput messageOutput)
        {
            var result = Globals.LState_Success.Clone();

            if (request.ExportHtmlPack)
            {
                var exportPacks = (from path in modelResult.Paths
                                   join exportPack in modelResult.HTMLExportPacks on path.ID_Object equals exportPack.ID_Object
                                   select new { NamePath = path.Name_Other, IdExportPack = exportPack.ID_Other }).GroupBy(pack => new { pack.NamePath, pack.IdExportPack }).Select(grp =>
                                     new
                                     {
                                         NamePath = grp.Key.NamePath,
                                         IdPack = grp.Key.IdExportPack
                                     });

                var restClient = new RestClient(serviceHost.ServiceUrl);
                foreach (var exportPack in exportPacks)
                {

                    var restRequest = new RestRequest("copyToMediaStore", Method.POST);

                    messageOutput?.OutputInfo($"Export to {exportPack.NamePath}...");
                    restRequest.AddParameter(nameof(MediaItemPathMap.PathDst), exportPack.NamePath);
                    restRequest.AddParameter(nameof(MediaItemPathMap.IdFile), exportPack.IdPack);
                    messageOutput?.OutputInfo($"Download Zip-File...");
                    IRestResponse<MediaItemPathMap> response = restClient.Execute<MediaItemPathMap>(restRequest);
                    messageOutput?.OutputInfo($"Downloaded Zip-File.");

                    result = response.Data.Result;
                    var fileName = Path.GetFileName(result.Additional2);
                    if (result.Name != Globals.LState_Success.Name &&
                        result.Name != Globals.LState_Nothing.Name)
                    {
                        result = Globals.LState_Error.Clone();
                        return result;
                    }

                    try
                    {
                        using (ZipArchive zipFile = ZipFile.OpenRead(result.Additional2))
                        {
                            messageOutput?.OutputInfo($"Extract {zipFile.Entries.Count} Files...");
                            zipFile.ExtractToDirectory(exportPack.NamePath, true);
                            messageOutput?.OutputInfo($"Extracted Files.");
                        }
                        File.Delete(result.Additional2);
                    }
                    catch (Exception ex)
                    {

                        result = Globals.LState_Error.Clone();
                        result.Additional1 = $"Error while extracting Zip-File {result.Additional2}: {ex.Message}";
                    }
                }

            }

            return result;
        }



        private clsOntologyItem ExportMedia(StringBuilder contentBuilder, clsObjectRel configToPath, clsObjectRel configToUrl, MediaServiceHost serviceHost)
        {
            var patternMedia = "[0-9a-fA-F]{32}";
            var patternMediaPart = "(src|href)=\"(../|/)?Resources/UserGroupRessources/[0-9a-fA-F]{2}/[0-9a-fA-F]{2}/" + patternMedia + "(\\.[a-zA-Z0-9]{3,4})?";

            var result = Globals.LState_Success.Clone();
            var regexMedia = new Regex(patternMedia);
            var regexMediaPart = new Regex(patternMediaPart);
            var matchesMediaParts = regexMediaPart.Matches(contentBuilder.ToString());

            var rootPath = Path.Combine(configToPath.Name_Other, "Ressources");

            foreach (Match matchMediaPart in matchesMediaParts)
            {
                var matchMedia = regexMedia.Match(matchMediaPart.Value);

                if (matchMedia.Success)
                {
                    result = ExportFile(rootPath, matchMedia.Value, serviceHost);

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var attribute = "src";
                    if (matchMediaPart.Value.StartsWith("href"))
                    {
                        attribute = "href";
                    }
                    contentBuilder.Replace(matchMediaPart.Value, attribute + "=\"" + configToUrl.Name_Other + "/Ressources/" +
                        matchMedia.Value.Substring(0, 2) + "/" +
                        matchMedia.Value.Substring(2, 2) + "/" +
                        result.Additional1);

                }
            }



            return result;
        }

        private clsOntologyItem ExportFile(string rootPath, string idFile, MediaServiceHost serviceHost)
        {
            var result = Globals.LState_Success.Clone();
            var filePath = Path.Combine(rootPath, idFile.Substring(0, 2), idFile.Substring(2, 2));

            //var result = mediaStoreConnector.SaveFileToManagedMedia(fileItem, fileItem.Additional1);
            var restClient = new RestClient(serviceHost.ServiceUrl);
            var restRequest = new RestRequest("copyToMediaStore", Method.POST);
            restRequest.AddParameter(nameof(MediaItemPathMap.PathDst), filePath);
            restRequest.AddParameter(nameof(MediaItemPathMap.IdFile), idFile);
            IRestResponse<MediaItemPathMap> response = restClient.Execute<MediaItemPathMap>(restRequest);

            if (response.Data == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "The MediaService is not reachable!";
                return result;
            }
            result = response.Data.Result;
            result.Additional1 = Path.GetFileName(result.Additional2);
            if (result.Name != Globals.LState_Success.Name &&
                result.Name != Globals.LState_Nothing.Name)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "Error while getting the File!";
                return result;
            }

            return result;
        }

        private async Task<ResultItem<List<TypedTag>>> WriteMetaJson(AnonymousItem anonymousItem,
                List<AnonymousItem> anonymousItems,
                clsObjectRel urlRel,
                clsObjectRel pathRel,
                HtmlEditorConnector connector,
                GetModelRawResult modelResult,
                IMessageOutput messageOutput,
                List<INameTransform> nameTransformProviders)
        {
            var taskResult = await Task.Run<ResultItem<List<TypedTag>>>(async () =>
            {
                var result = new ResultItem<List<TypedTag>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<TypedTag>()
                };

                var tagsTask = await connector.GetTags(anonymousItem.AnonymousAllowed.GUID);

                var tags = tagsTask.TypedTags.Where(typedTag => typedTag.IdTagParent != "902b2cd20fe64f4992fd97bcf28e2dad"
                    && typedTag.IdTagParent != "f62cf1ccee51475f9c007094f1809b56"
                    && typedTag.IdTagParent != "d84fa125dbce44b091539abeb66ad27f"
                    && typedTag.IdTagParent != "d0f582137db44882bd15962163015d13").OrderBy(typedTag => typedTag.OrderId).ThenBy(typedTag => typedTag.NameTag);
                var otherRefTypes = tags.Where(typedTag => typedTag.IdTagParent != "ce29760f023043daa19d91c6d82a8437" && typedTag.IdTagParent != "3bb4b05d8fcb48b7a1ac0fb42a265c38").GroupBy(typedTag => new { Id = typedTag.IdTagParent, Name = typedTag.NameTagParent }).OrderBy(refGroup => refGroup.Key.Name).ToList();

                var htmlDocs = await connector.GetHtmlDocumentsByRefs(tags.Select(tag => new clsOntologyItem { GUID = tag.IdTag }).ToList());
                var htmlDocsAllowed = (from htmlDocRel in htmlDocs.HtmlDocumentsToRef
                                       join allowedDoc in anonymousItems on htmlDocRel.ID_Object equals allowedDoc.AnonymousAllowed.GUID
                                       select htmlDocRel);

                result.Result.AddRange(tags);
                var tagsWithReferences = (from tag in tags
                                          join htmlDoc in htmlDocsAllowed on tag.IdTag equals htmlDoc.ID_Other into htmlDocs1
                                          from htmlDoc in htmlDocs1.DefaultIfEmpty()
                                          select new TypedTagWithHtmlPage(tag, htmlDoc != null ? urlRel.Name_Other + (urlRel.Name_Other.EndsWith("/") ? "" : "/") + "Index.html" + "?idDoc=" + htmlDoc.ID_Object : "", ""));

                var sceneTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "ce29760f023043daa19d91c6d82a8437");

                if (sceneTags.Any())
                {
                    anonymousItem.TypedReferences.Add(new Reference
                    {
                        Id = "ce29760f023043daa19d91c6d82a8437",
                        Name = "Szenen",
                        Tags = sceneTags.ToList()
                    });
                }

                var termTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "3bb4b05d8fcb48b7a1ac0fb42a265c38");

                if (termTags.Any())
                {
                    anonymousItem.TypedReferences.Add(new Reference
                    {
                        Id = "3bb4b05d8fcb48b7a1ac0fb42a265c38",
                        Name = "Schlagwörter",
                        Tags = termTags.OrderBy(term => term.NameTag).ToList()
                    });
                }

                otherRefTypes.ForEach(refGroup =>
                {
                    var reference = new Reference
                    {
                        Id = refGroup.Key.Id,
                        Name = refGroup.Key.Name
                    };

                    reference.Tags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == refGroup.Key.Id).ToList();
                    anonymousItem.TypedReferences.Add(reference);
                });

                var refItemClassesWithProvider = (from refItemClass in anonymousItem.TypedReferences.SelectMany(refItem => refItem.Tags).GroupBy(grp => grp.IdTagParent).Select(grp => grp.Key).Where(cls => cls != null)
                                                  from transformProvider in nameTransformProviders
                                                  where transformProvider.IsResponsible(refItemClass)
                                                  select new { refItemClass, transformProvider }).ToList();

                foreach (var refItemClassWithProvider in refItemClassesWithProvider)
                {
                    var refItems = anonymousItem.TypedReferences.SelectMany(refItem => refItem.Tags).Where(refItem => refItem.IdTagParent == refItemClassWithProvider.refItemClass).Select(typedTag =>
                        new clsOntologyItem
                        {
                            GUID = typedTag.IdTag,
                            Name = typedTag.NameTag,
                            GUID_Parent = typedTag.IdTagParent,
                            Type = typedTag.TagType
                        }).ToList();
                    var refItemsResult = await refItemClassWithProvider.transformProvider.TransformNames(refItems, false);
                    foreach (var typedTagAndRefItem in (from typedTag in anonymousItem.TypedReferences.SelectMany(refItem => refItem.Tags)
                                                        join refItem in refItemsResult.Result on typedTag.IdTag equals refItem.GUID
                                                        select new { typedTag, refItem }))
                    {
                        typedTagAndRefItem.typedTag.NameTag = typedTagAndRefItem.refItem.Name.Replace("\"", "'");
                        typedTagAndRefItem.typedTag.NameTypedTag = typedTagAndRefItem.refItem.Name.Replace("\"", "'");
                    }

                }

                messageOutput?.OutputMessage($"Create Meta...", MessageOutputType.Info);
                var filePath = System.IO.Path.Combine(pathRel.Name_Other, anonymousItem.AnonymousAllowed.GUID + ".json");
                System.IO.File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(anonymousItem));
                messageOutput?.OutputMessage($"Created Meta", MessageOutputType.Info);

                return result;
            });

            return taskResult;
        }

        private async Task<clsOntologyItem> WriteMetaTree(string idItem, HtmlEditorConnector connector, clsObjectRel urlRel, clsObjectRel pathRel, GetModelRawResult modelResult, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var hierarchyResult = await connector.GetHtmlHierarchy(idItem);

                if (hierarchyResult.Result.GUID == connector.Globals.LState_Error.GUID)
                {
                    return Globals.LState_Error.Clone();
                }

                var result = new KendoTreeData
                {
                    dataSource = new KendoHierarchicalDataSource
                    {
                        transport = new KendoTransport
                        {
                            read = new KendoTransportCRUD
                            {
                                dataType = "JSON",
                                url = urlRel.Name_Other + (urlRel.Name_Other.EndsWith("/") ? "" : "/") + idItem + "_tree.json"
                            }
                        },
                        schema = new KendoHierarchicalSchema
                        {
                            model = new KendoHierarchicalModel
                            {
                                id = nameof(KendoTreeNode.NodeId),
                                children = nameof(KendoTreeNode.SubNodes),
                                hasChildren = nameof(KendoTreeNode.HasChildren)
                            }
                        }
                    },
                    data = new List<KendoTreeNode>()
                };

                var htmlDoc = hierarchyResult.Hierarchy.FirstOrDefault(hierarchyItem => hierarchyItem.ID_Object == hierarchyResult.HtmlDocumentOfRef.GUID);

                if (htmlDoc == null)
                {
                    return Globals.LState_Success.Clone();
                }
                var kendoTreeNode = new KendoTreeNode
                {
                    NodeId = htmlDoc.ID_Object,
                    NodeName = htmlDoc.Name_Object,

                };

                kendoTreeNode.SubNodes = GetTreeNodes(hierarchyResult.Hierarchy, kendoTreeNode);

                result.data.Add(kendoTreeNode);

                messageOutput.OutputMessage("Create TreeData...", MessageOutputType.Info);
                var filePath = System.IO.Path.Combine(pathRel.Name_Other, idItem + "_tree.json");
                System.IO.File.WriteAllText(filePath, Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8);
                messageOutput.OutputMessage("Created TreeData", MessageOutputType.Info);

                return Globals.LState_Success.Clone();
            });

            return taskResult;
        }

        private static List<KendoTreeNode> GetTreeNodes(List<clsObjectRel> hierarchy, KendoTreeNode parentNode)
        {
            var subNodes = hierarchy.Where(subNode => subNode.ID_Object == parentNode.NodeId).OrderBy(subNode => subNode.OrderID).Select(subNode => new KendoTreeNode
            {
                NodeId = subNode.ID_Other,
                NodeName = subNode.Name_Other
            }).ToList();

            parentNode.HasChildren = subNodes.Any();
            subNodes.ForEach(subNode =>
            {
                subNode.SubNodes = GetTreeNodes(hierarchy, subNode);
            });

            return subNodes.Any() ? subNodes : null;
        }

        public ExportHtmlDocController(Globals globals) : base(globals)
        {
        }
    }

    public class MediaItemPathMap
    {
        public string PathSrc { get; set; }
        public string PathDst { get; set; }
        public bool DeleteExistingDst { get; set; }
        public string IdFile { get; set; }
        public string NameFile { get; set; }
        public clsOntologyItem Result { get; set; }
    }
}
