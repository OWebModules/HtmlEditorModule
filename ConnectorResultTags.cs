﻿using OntologyClasses.BaseClasses;
using System.Collections.Generic;
using TypedTaggingModule.Models;

namespace HtmlEditorModule
{
    public class ConnectorResultTags
    {
        public clsOntologyItem Result { get; set; }
        public List<Reference> References { get; set; }
    }
}
