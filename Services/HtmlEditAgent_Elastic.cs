﻿using ElasticSearchNestConnector;
using HtmlEditorModule.Factories;
using HtmlEditorModule.Models;
using HtmlEditorModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HtmlEditorModule.Services
{
    public class HtmlEditAgent_Elastic :ElasticBaseAgent
    {

        private object controllerLocker = new object();

        private clsOntologyItem oItemRef;
        public clsOntologyItem OItemRef
        {
            get { return oItemRef; }
            set
            {
                oItemRef = value;
            }
        }

        private bool leftRightHtmlDocRef;


        private OntologyModDBConnector dbReader_HtmlDoc;
        private OntologyModDBConnector dbReader_HtmlCode;
        private OntologyModDBConnector dbReader_HtmlStyle;

        private Thread getHtmlDocumentAsync;

        private clsRelationConfig relationConfig;
        private clsTransaction transaction;

        private ResultHtmlDoc resultHtmlDoc;
        public ResultHtmlDoc ResultHtmlDoc
        {
            get { return resultHtmlDoc; }
            set
            {
                resultHtmlDoc = value;
            }
        }

        private ResultClassPath resultClassPath;
        public ResultClassPath ResultClassPath
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultClassPath;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    resultClassPath = value;
                }
            }
        }

        private ResultHtmlObject resultHtmlObject;
        public ResultHtmlObject ResultHtmlObject
        {
            get
            {
                lock (controllerLocker)
                {
                    return resultHtmlObject;
                }
            }
            set
            {
                lock (controllerLocker)
                {
                    resultHtmlObject = value;
                }
            }
        }

        public async Task<ResultHtmlDoc> GetHtmlDocument(clsOntologyItem oItemRef)
        {
            var taskResult = await Task.Run<ResultHtmlDoc>(() =>
            {
                this.oItemRef = oItemRef;
                return GetRelatedHtmlDocumentAsync();
            });

            return taskResult;
        }

        public async Task<ResultItem<ResultHtmlTemplates>> GetHtmlDocumentOfTemplate(clsOntologyItem oItemRef)
        {
            var taskResult = await Task.Run<ResultItem<ResultHtmlTemplates>>(() =>
            {
                var result = new ResultItem<ResultHtmlTemplates>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ResultHtmlTemplates()
                };

                var searchTemplates = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemRef.GUID_Parent,
                        ID_Parent_Object = Config.LocalData.Class_HTML_Templates.GUID,
                        ID_RelationType = Config.LocalData.RelationType_belonging_Class.GUID
                    }
                };

                var dbReaderGetTemplates = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGetTemplates.GetDataObjectRel(searchTemplates);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting templates of ref-class";
                    return result;
                }

                result.Result.ClassToTemplates = dbReaderGetTemplates.ObjectRels;

                var searchHtmlTemplates = dbReaderGetTemplates.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Other = rel.ID_Object,
                    ID_RelationType = Config.LocalData.RelationType_reference_to.GUID,
                    ID_Parent_Object = Config.LocalData.Class_HTML_Document.GUID
                }).ToList();

                if (searchHtmlTemplates.Any())
                {
                    var dbReaderHtmlDocumentsOfTemplates = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderHtmlDocumentsOfTemplates.GetDataObjectRel(searchHtmlTemplates);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Html-documents of templates";
                        return result;
                    }

                    result.Result.TemplatesToHtmlDocuments = dbReaderHtmlDocumentsOfTemplates.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ResultHtmlDoc>>> GetHtmlDocumentsFull(List<clsOntologyItem> refItems)
        {
            var taskResult = await Task.Run<ResultItem<List<ResultHtmlDoc>>>(() =>
            {
                var result = new ResultItem<List<ResultHtmlDoc>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<ResultHtmlDoc>()
                };

                var searchHtmlDocuments = refItems.Select(refItem => new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_Parent_Object = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType
                }).ToList();

                var dbReaderHtmlDocuments = new OntologyModDBConnector(globals);

                if (searchHtmlDocuments.Any())
                {
                    result.ResultState = dbReaderHtmlDocuments.GetDataObjectRel(searchHtmlDocuments);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Html-Document objects!";
                        return result;
                    }

                }

                

                var searchHtmlCode = dbReaderHtmlDocuments.ObjectRels.Select(htmlDoc => new clsObjectAtt
                {
                    ID_Object = htmlDoc.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_HTML_Document_HTML.ID_AttributeType
                }).ToList();

                var dbReaderHtmlCode = new OntologyModDBConnector(globals);

                if (searchHtmlCode.Any())
                {
                    result.ResultState = dbReaderHtmlCode.GetDataObjectAtt(searchHtmlCode);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Html Attributes!";
                        return result;
                    }
                }

                var searchStyle = dbReaderHtmlDocuments.ObjectRels.Select(htmlDoc => new clsObjectAtt
                {
                    ID_Object = htmlDoc.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_HTML_Document_Style.ID_AttributeType
                }).ToList();

                var dbReaderStyle = new OntologyModDBConnector(globals);

                if (searchStyle.Any())
                {
                    result.ResultState = dbReaderStyle.GetDataObjectAtt(searchStyle);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Style Attributes!";
                        return result;
                    }
                }

                result.Result = (from documentRel in dbReaderHtmlDocuments.ObjectRels
                                join htmlAtt in dbReaderHtmlCode.ObjAtts on documentRel.ID_Object equals htmlAtt.ID_Object
                                select new ResultHtmlDoc
                                {
                                    Result = result.ResultState,
                                    RefItem = new clsOntologyItem
                                    {
                                        GUID = documentRel.ID_Other,
                                        Name = documentRel.Name_Other,
                                        GUID_Parent = documentRel.ID_Parent_Other,
                                        Type = documentRel.Ontology
                                    },
                                    HtmlDocument = new clsOntologyItem
                                    {
                                        GUID = documentRel.ID_Object,
                                        Name = documentRel.Name_Object,
                                        GUID_Parent = documentRel.ID_Parent_Object,
                                        Type = globals.Type_Object
                                    },
                                    HtmlOAttribute = htmlAtt,
                                    StylesOAttribute = dbReaderStyle.ObjAtts.Where(att => att.ID_Object == documentRel.ID_Object).ToList()
                                }).ToList();

                return result;
            });
            return taskResult;
        }

        public async Task<ResultGetHtmlDocuments> GetHtmlDocuments(List<clsOntologyItem> refItems)
        {
            var taskResult = await Task.Run<ResultGetHtmlDocuments>(() =>
            {
                var result =
                new ResultGetHtmlDocuments
                {
                    Result = globals.LState_Success.Clone()
                };

                var search = refItems.Select(refItem => new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_Parent_Object = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                if (search.Any())
                {
                    result.Result = dbReader.GetDataObjectRel(search);
                }


                if (result.Result.GUID == globals.LState_Success.GUID)
                {
                    result.HtmlDocumentsToRef = dbReader.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        private ResultHtmlDoc GetRelatedHtmlDocumentAsync()
        {
            var result = new ResultHtmlDoc
            {
                Result = globals.LState_Success.Clone()
            };

            if (oItemRef.GUID_Parent != Config.LocalData.Class_HTML_Document.GUID)
            {
                result.Result = GetSubData_001_HtmlDocument();
                if (leftRightHtmlDocRef)
                {
                    result.HtmlDocument = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Other,
                        Name = objRel.Name_Other,
                        GUID_Parent = objRel.ID_Parent_Other,
                        Type = objRel.Ontology
                    }).FirstOrDefault();


                }
                else
                {
                    result.HtmlDocument = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Object,
                        Name = objRel.Name_Object,
                        GUID_Parent = objRel.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).FirstOrDefault();
                }
            }
            else
            {
                result.HtmlDocument = oItemRef;
            }

            if (result.HtmlDocument != null)
            {
                result.Result = GetSubData_002_HTMLCode(result.HtmlDocument);
            }
            else
            {
                dbReader_HtmlCode.ObjAtts.Clear();
            }

            if (result.Result.GUID == globals.LState_Success.GUID)
            {
                result.HtmlOAttribute = dbReader_HtmlCode.ObjAtts.FirstOrDefault();
            }

            if (result.HtmlDocument != null)
            {
                result.Result = GetSubData_003_Style(result.HtmlDocument);
            }
            else
            {
                dbReader_HtmlStyle.ObjAtts.Clear();
            }

            if (result.Result.GUID == globals.LState_Success.GUID)
            {
                result.StylesOAttribute = dbReader_HtmlStyle.ObjAtts;
            }

            ResultHtmlDoc = result;
            return result;
        }

        private clsOntologyItem GetSubData_001_HtmlDocument()
        {
            var searchHtmlDocument = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = Config.LocalData.Class_HTML_Document.GUID
                }
            };

            var result = dbReader_HtmlDoc.GetDataObjectRel(searchHtmlDocument);

            if (result.GUID == globals.LState_Error.GUID) return result;

            if (dbReader_HtmlDoc.ObjectRels.Any())
            {

                leftRightHtmlDocRef = true;
                return result;
            }

            searchHtmlDocument = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_Parent_Object = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_Class_Left
                }
            };

            result = dbReader_HtmlDoc.GetDataObjectRel(searchHtmlDocument);
            leftRightHtmlDocRef = false;
            return result;
        }


        public async Task<ResultItem<clsObjectAtt>> GetHtmlCode(clsOntologyItem htmlDoc)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectAtt>>(() =>
          {
              var result = new ResultItem<clsObjectAtt>
              {
                  ResultState = globals.LState_Success.Clone()
              };

              var searchHtmlCode = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_Object = htmlDoc.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_HTML_Document_HTML.ID_AttributeType
                }
            };

              var dbReaderHtmlCode = new OntologyModDBConnector(globals);

              result.ResultState = dbReaderHtmlCode.GetDataObjectAtt(searchHtmlCode);

              if (result.ResultState.GUID == globals.LState_Error.GUID)
              {
                  result.ResultState.Additional1 = "Error while getting Html-Code!";
                  return result;
              }

              result.Result = dbReaderHtmlCode.ObjAtts.FirstOrDefault();

              return result;
          });
            return taskResult;
        }

        private clsOntologyItem GetSubData_002_HTMLCode(clsOntologyItem htmlDoc)
        {
            var searchHtmlCode = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_Object = htmlDoc.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_HTML_Document_HTML.ID_AttributeType
                }
            };

            var result = dbReader_HtmlCode.GetDataObjectAtt(searchHtmlCode);

            return result;
        }

        private clsOntologyItem GetSubData_003_Style(clsOntologyItem htmlDoc)
        {
            var searchStyle = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_Object = htmlDoc.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_HTML_Document_Style.ID_AttributeType
                }
            };

            var result = dbReader_HtmlStyle.GetDataObjectAtt(searchStyle);

            return result;
        }

        public ResultHtmlDoc SaveDocName(clsOntologyItem oItemHtmlDocument, clsOntologyItem oItemRef, string name)
        {
            var resultSaveDoc = new ResultHtmlDoc
            {
                Result = globals.LState_Success.Clone(),
                HtmlDocument = oItemHtmlDocument,
                RefItem = oItemRef
            };

            transaction.ClearItems();

            if (resultSaveDoc.HtmlDocument != null)
            {
                resultSaveDoc.HtmlDocument.Name = name;
                resultSaveDoc.Result = transaction.do_Transaction(resultSaveDoc.HtmlDocument);
            }
            else
            {
                resultSaveDoc.HtmlDocument = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = oItemRef.Name,
                    GUID_Parent = Config.LocalData.Class_HTML_Document.GUID,
                    Type = globals.Type_Object
                };

                resultSaveDoc.Result = transaction.do_Transaction(resultSaveDoc.HtmlDocument);

                if (resultSaveDoc.Result.GUID == globals.LState_Success.GUID)
                {
                    var refRelHtmlDoc = relationConfig.Rel_ObjectRelation(resultSaveDoc.HtmlDocument, oItemRef, Config.LocalData.RelationType_reference_to);
                    resultSaveDoc.Result = transaction.do_Transaction(refRelHtmlDoc, true);
                    if (resultSaveDoc.Result.GUID == globals.LState_Success.GUID)
                    {
                        resultSaveDoc = SaveHtmlContent(resultSaveDoc.HtmlDocument, oItemRef, "");
                    }

                }
            }

            return resultSaveDoc;
        }



        public async Task<clsOntologyItem> ArchiveHtmlContent(clsObjectAtt oAttributeHtmlCode)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var appDBSeletor = new clsUserAppDBSelector(globals.Server,
                globals.Port,
                "HtmlEditorController".ToLower(),
                globals.SearchRange,
                globals.Session);

                var appDBUpdater = new clsUserAppDBUpdater(appDBSeletor);

                var doc = new clsAppDocuments
                {
                    Id = globals.NewGUID,
                    Dict = new Dictionary<string, object>()
                };

                doc.Dict = new Dictionary<string, object>();
                doc.Dict.Add(nameof(oAttributeHtmlCode.ID_Attribute), oAttributeHtmlCode.ID_Attribute);
                doc.Dict.Add(nameof(oAttributeHtmlCode.ID_Object), oAttributeHtmlCode.ID_Object);
                doc.Dict.Add(nameof(oAttributeHtmlCode.Val_String), oAttributeHtmlCode.Val_String);
                doc.Dict.Add("datestamp", DateTime.Now);

                var result = appDBUpdater.SaveDoc(new List<clsAppDocuments> { doc }, "code");

                return result;
            });

            return taskResult;
        }



        public ResultHtmlDoc SaveHtmlContent(clsOntologyItem oItemHtmlDocument, clsOntologyItem oItemRef, string html)
        {
            transaction.ClearItems();
            var resultSaveHtmlContent = new ResultHtmlDoc
            {
                Result = globals.LState_Success,
                RefItem = oItemRef,

            };

            resultSaveHtmlContent.HtmlDocument = oItemHtmlDocument;

            if (oItemHtmlDocument != null)
            {
                var saveAttribute = relationConfig.Rel_ObjectAttribute(oItemHtmlDocument, Config.LocalData.AttributeType_HTML, html);
                var resultTask = ArchiveHtmlContent(saveAttribute);
                resultSaveHtmlContent.Result = transaction.do_Transaction(saveAttribute, true);
                resultSaveHtmlContent.HtmlOAttribute = transaction.OItem_Last.OItemObjectAtt;
                return resultSaveHtmlContent;
            }
            else
            {

                var resultSave = SaveDocName(resultSaveHtmlContent.HtmlDocument, oItemRef, oItemRef.Name);
                if (resultSave.Result.GUID == globals.LState_Error.GUID) return resultSave;

                if (oItemRef.GUID_Parent == Config.LocalData.Class_HTML_Document.GUID)
                {
                    var saveAttribute = relationConfig.Rel_ObjectAttribute(resultSave.HtmlDocument, Config.LocalData.AttributeType_HTML, html);
                    resultSave.Result = transaction.do_Transaction(saveAttribute, true);
                    return resultSave;
                }
                else
                {


                    if (resultSave.Result.GUID == globals.LState_Success.GUID)
                    {
                        var saveAttribute = relationConfig.Rel_ObjectAttribute(resultSave.HtmlDocument, Config.LocalData.AttributeType_HTML, html);
                        resultSave.Result = transaction.do_Transaction(saveAttribute, true);
                        if (resultSave.Result.GUID == globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                        }


                    }

                    if (resultSave.Result.GUID == globals.LState_Error.GUID)
                    {
                        resultSave.HtmlOAttribute = dbReader_HtmlCode.ObjAtts.FirstOrDefault();
                    }
                    else
                    {
                        resultSave.HtmlOAttribute = transaction.OItem_Last.OItemObjectAtt;
                    }

                    return resultSave;
                }

            }
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> ids)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);
                result.ResultState = dbReader.GetDataObjects(ids.Select(id => new clsOntologyItem { GUID = id }).ToList());

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the objects!";
                    return result;
                }

                result.Result = dbReader.Objects1;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjectItemsWithGuid(List<clsOntologyItem> objectItems)
        {

            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var dbReader = new OntologyModDBConnector(globals);
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = dbReader.GetDataObjects(objectItems)
                };

                result.Result = dbReader.Objects1;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetModelRawResult>> GetExportAnonymousHtmlModel(ExportAnonymousHtmlRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelRawResult>>(() =>
           {
               var result = new ResultItem<GetModelRawResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetModelRawResult()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = Config.LocalData.Class_ExportAnonymousHTMLModule.GUID
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetExportAnonymousHtmlModel(result.Result, globals, nameof(result.Result.RootConfig));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_contains_ExportAnonymousHTMLModule.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_contains_ExportAnonymousHTMLModule.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Sub-Configs!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }


               var searchIndexCommandLineRun = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_index_Comand_Line__Run_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_index_Comand_Line__Run_.ID_Class_Right
               }).ToList();

               var dbReaderIndexCommandLineRun = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderIndexCommandLineRun.GetDataObjectRel(searchIndexCommandLineRun);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Index CommandLineRun!";
                   return result;
               }

               result.Result.IndexCommandLineRuns = dbReaderIndexCommandLineRun.ObjectRels;

               result.ResultState = ValidationController.ValidateGetExportAnonymousHtmlModel(result.Result, globals, nameof(result.Result.IndexCommandLineRuns));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }


               var searchForDocsCommandLineRun = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Export.Config.LocalData.ClassRel_ExportAnonymousHTMLModule_for_documents_Comand_Line__Run_.ID_RelationType,
                   ID_Parent_Other = Export.Config.LocalData.ClassRel_ExportAnonymousHTMLModule_for_documents_Comand_Line__Run_.ID_Class_Right
               }).ToList();

               var dbReaderForDocsCommandLineRun = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderForDocsCommandLineRun.GetDataObjectRel(searchForDocsCommandLineRun);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the ForDocs CommandLineRun!";
                   return result;
               }

               result.Result.ForDocsCommandLineRuns = dbReaderForDocsCommandLineRun.ObjectRels;

               result.ResultState = ValidationController.ValidateGetExportAnonymousHtmlModel(result.Result, globals, nameof(result.Result.ForDocsCommandLineRuns));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchPath = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_export_to_Path.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_export_to_Path.ID_Class_Right
               }).ToList();

               var dbReaderPath = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPath.GetDataObjectRel(searchPath);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Export-Paths!";
                   return result;
               }

               result.Result.Paths = dbReaderPath.ObjectRels;
               result.ResultState = ValidationController.ValidateGetExportAnonymousHtmlModel(result.Result, globals, nameof(result.Result.Paths));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchUrl = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_Root_Resource_Url.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_Root_Resource_Url.ID_Class_Right
               }).ToList();

               var dbReaderUrl = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderUrl.GetDataObjectRel(searchUrl);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Urls!";
                   return result;
               }

               result.Result.Urls = dbReaderUrl.ObjectRels.ToList();
               result.ResultState = ValidationController.ValidateGetExportAnonymousHtmlModel(result.Result, globals, nameof(result.Result.Paths));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }


               var searchHTMLExportPack = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_HTML_Pack_File.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_HTML_Pack_File.ID_Class_Right
               }).ToList();

               var dbReaderHTMLExportPack = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderHTMLExportPack.GetDataObjectRel(searchHTMLExportPack);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the HTML-Export Packs!";
                   return result;
               }

               result.Result.HTMLExportPacks = dbReaderHTMLExportPack.ObjectRels;

               var searchAnonymous = result.Result.Configs.Select(config =>
                   new clsObjectRel
                   {
                       ID_Object = config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_ExportAnonymousHTMLModule_belonging_Document.ID_RelationType
                   }).ToList();


               var dbReaderAnonymous = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderAnonymous.GetDataObjectRel(searchAnonymous);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the related documents!";
                   return result;
               }

               result.Result.ExportReportItems = dbReaderAnonymous.ObjectRels.Where(rel => rel.ID_Parent_Other == Export.Report.Config.LocalData.Class_Export_Report_Json.GUID).ToList();

               var anonymousRel = (from config in result.Result.Configs
                                   join anonymousRelItem in dbReaderAnonymous.ObjectRels.Where(rel => rel.ID_Parent_Other != Export.Report.Config.LocalData.Class_Export_Report_Json.GUID) on config.GUID equals anonymousRelItem.ID_Object
                                   select new
                                   {
                                       config,
                                       anonymousRelItem,
                                       anonymousItem = new clsOntologyItem
                                       {
                                           GUID = anonymousRelItem.ID_Other,
                                           Name = anonymousRelItem.Name_Other,
                                           GUID_Parent = anonymousRelItem.ID_Parent_Other,
                                           Type = anonymousRelItem.Ontology
                                       }
                                   });
               var searchAnonymousRefs = anonymousRel.Where(anonymousItem => anonymousItem.anonymousItem.GUID_Parent == Config.LocalData.Class_HTML_Document.GUID).Select(anonymousItem => new clsObjectRel
               {
                   ID_Object = anonymousItem.anonymousItem.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType
               }).ToList();

               var searchExportStates = result.Result.Configs.Select(config => new clsObjectRel
                   {
                       ID_Other = request.IdConfig,
                       ID_RelationType = Export.Config.LocalData.RelationType_belongs_to.GUID,
                       ID_Parent_Object = Config.LocalData.Class_Export_Stat__Anonymous_.GUID
                   }).ToList();

               var dbReaderExportState = new OntologyModDBConnector(globals);

               if (searchExportStates.Any())
               {
                   result.ResultState = dbReaderExportState.GetDataObjectRel(searchExportStates);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the export stat!";
                       return result;
                   }
               }

               var searchExportTimeStamps = dbReaderExportState.ObjectRels.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Object,
                   ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID
               }).ToList();

               var dbReaderExportDateTimeStamp = new OntologyModDBConnector(globals);

               if (searchExportTimeStamps.Any())
               {
                   result.ResultState = dbReaderExportDateTimeStamp.GetDataObjectAtt(searchExportTimeStamps);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the export stat timestamps!";
                       return result;
                   }
               }

               if (dbReaderExportDateTimeStamp.ObjAtts.Any())
               {
                   foreach (var config in result.Result.Configs)
                   {
                       var stamp = (from stat in dbReaderExportState.ObjectRels.Where(rel => rel.ID_Other == config.GUID)
                                    join timeStamp in dbReaderExportDateTimeStamp.ObjAtts on stat.ID_Object equals timeStamp.ID_Object
                                    select new { stat, timeStamp }).OrderByDescending(timeStamp => timeStamp.timeStamp.Val_Datetime).FirstOrDefault();
                       if (stamp != null)
                       {
                           result.Result.StatsToConfigs.Add(stamp.stat);
                           result.Result.StatsDateTime.Add(stamp.timeStamp);
                       }
                   }
               }

               var searchExportLastEdit = result.Result.Configs.Select(config => new clsObjectAtt
               {
                   ID_Object = config.GUID,
                   ID_AttributeType = Export.Config.LocalData.AttributeType_Export_only_changed.GUID
               }).ToList();

               var dbReaderExportLastEdit = new OntologyModDBConnector(globals);

               if (searchExportLastEdit.Any())
               {
                   result.ResultState = dbReaderExportLastEdit.GetDataObjectAtt(searchExportLastEdit);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the flag for export last edit!";
                       return result;
                   }

                   result.Result.ExportLastEdit = dbReaderExportLastEdit.ObjAtts;
               }

               if (searchAnonymousRefs.Any())
               {
                   var dbReaderAnonymousRef = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderAnonymousRef.GetDataObjectRel(searchAnonymousRefs);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the References of Anonymous!";
                       return result;
                   }

                   foreach (var anonymous in anonymousRel)
                   {
                       var anonymousItem = new AnonymousItem
                       {
                           ConfigItem = anonymous.config,
                           AnonymousAllowed = anonymous.anonymousItem,
                           RefItems = dbReaderAnonymousRef.ObjectRels.Where(rel => rel.ID_Object == anonymous.anonymousItem.GUID).Select(rel => new clsOntologyItem
                           {
                               GUID = rel.ID_Other,
                               Name = rel.Name_Other,
                               GUID_Parent = rel.ID_Parent_Other,
                               Type = rel.Ontology
                           }).ToList()
                       };
                       result.Result.AnonymousItems.Add(anonymousItem);
                   }
               }

               var searchHtmls = anonymousRel.Where(anonymousItem => anonymousItem.anonymousItem.GUID_Parent != Config.LocalData.Class_HTML_Document.GUID).Select(anonymousItem => new clsObjectRel
               {
                   ID_Other = anonymousItem.anonymousItem.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType,
                   ID_Parent_Object = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_Class_Left
               }).ToList();

               var dbReaderHtmls = new OntologyModDBConnector(globals);
               if (searchHtmls.Any())
               {
                   result.ResultState = dbReaderHtmls.GetDataObjectRel(searchHtmls);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the HTMLs of the Anonymoues";
                       return result;
                   }

                   var anonymousRels2 = (from config in result.Result.Configs
                                         join htmlRel in dbReaderHtmls.ObjectRels on config.GUID equals htmlRel.ID_Object
                                         select new { config, htmlRel });

                   foreach (var anonymous in anonymousRels2)
                   {
                       var anonymousItem = new AnonymousItem
                       {
                           ConfigItem = anonymous.config,
                           AnonymousAllowed = new clsOntologyItem
                           {
                               GUID = anonymous.htmlRel.ID_Object,
                               Name = anonymous.htmlRel.Name_Object,
                               GUID_Parent = anonymous.htmlRel.ID_Parent_Object,
                               Type = globals.Type_Object
                           },
                           RefItems = new List<clsOntologyItem>
                           {
                               new clsOntologyItem
                               {
                                   GUID = anonymous.htmlRel.ID_Other,
                                   Name = anonymous.htmlRel.Name_Other,
                                   GUID_Parent = anonymous.htmlRel.ID_Parent_Other,
                                   Type = globals.Type_Object
                               }
                           }
                       };
                       result.Result.AnonymousItems.Add(anonymousItem);
                   }

               }

               return result;
           });

            return taskResult;
        }



        public async Task<clsOntologyItem> SaveExportStat(List<clsOntologyItem> statItems, List<clsObjectRel> rels, List<clsObjectAtt> attributes)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                if (statItems.Any())
                {
                    result = dbWriter.SaveObjects(statItems);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Stat-Item!";
                        return result;
                    }
                }


                if (rels.Any())
                {
                    result = dbWriter.SaveObjRel(rels);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the relations!";
                        return result;
                    }
                }

                if (attributes.Any())
                {
                    result = dbWriter.SaveObjAtt(attributes);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the attributes!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<long>> GetNextOrderId(clsOntologyItem export)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
           {
               var result = new ResultItem<long>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = 1
               };

               var dbReaderOrder = new OntologyModDBConnector(globals);

               result.Result = dbReaderOrder.GetDataRelOrderId(new clsOntologyItem { GUID_Parent = Config.LocalData.Class_Export_Stat__Anonymous_.GUID }, export, Config.LocalData.RelationType_belongs_to, doAsc: false) + 1;


               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetRelatedHtmlDocumentWithoutCode(clsOntologyItem oItemRef)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone(),
                };

                if (oItemRef.GUID_Parent != Config.LocalData.Class_HTML_Document.GUID)
                {
                    result.ResultState = GetSubData_001_HtmlDocument();
                    if (leftRightHtmlDocRef)
                    {
                        result.Result = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                        {
                            GUID = objRel.ID_Other,
                            Name = objRel.Name_Other,
                            GUID_Parent = objRel.ID_Parent_Other,
                            Type = objRel.Ontology
                        }).FirstOrDefault();


                    }
                    else
                    {
                        result.Result = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                        {
                            GUID = objRel.ID_Object,
                            Name = objRel.Name_Object,
                            GUID_Parent = objRel.ID_Parent_Object,
                            Type = globals.Type_Object
                        }).FirstOrDefault();
                    }
                }
                else
                {
                    result.Result = oItemRef;
                }

                return result;
            });

            return taskResult;




        }

        public async Task<ResultItem<ExportReportModel>> GetExportReportModel(List<clsObjectRel> configsToExportReportConfigs)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ExportReportModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ExportReportModel
                    {
                        ConfigsToExportReportsConfig = configsToExportReportConfigs
                    }
                };

                var searchJsonRowTemplates = configsToExportReportConfigs.Select(configTo => new clsObjectAtt
                {
                    ID_Object = configTo.ID_Other,
                    ID_AttributeType = Export.Report.Config.LocalData.ClassAtt_Export_Report_Json_Json_Row_Template.ID_AttributeType
                }).ToList();

                var dbReaderJsonRowTemplates = new OntologyModDBConnector(globals);

                if (searchJsonRowTemplates.Any())
                {
                    result.ResultState = dbReaderJsonRowTemplates.GetDataObjectAtt(searchJsonRowTemplates);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Row-Templates!";
                        return result;
                    }

                    result.Result.JsonRowTemplates = dbReaderJsonRowTemplates.ObjAtts;
                }

                result.ResultState = ValidationController.ValidateGetExportReportModel(result.Result, globals, nameof(ExportReportModel.JsonRowTemplates));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchJsonTableTemplates = configsToExportReportConfigs.Select(configTo => new clsObjectAtt
                {
                    ID_Object = configTo.ID_Other,
                    ID_AttributeType = Export.Report.Config.LocalData.ClassAtt_Export_Report_Json_Json_Table_Template.ID_AttributeType
                }).ToList();

                var dbReaderJsonTableTemplates = new OntologyModDBConnector(globals);

                if (searchJsonTableTemplates.Any())
                {
                    result.ResultState = dbReaderJsonTableTemplates.GetDataObjectAtt(searchJsonTableTemplates);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Table-Templates!";
                        return result;
                    }

                    result.Result.JsonTableTemplates = dbReaderJsonTableTemplates.ObjAtts;
                }

                result.ResultState = ValidationController.ValidateGetExportReportModel(result.Result, globals, nameof(ExportReportModel.JsonTableTemplates));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchFilesFields = configsToExportReportConfigs.Select(configTo => new clsObjectRel
                {
                    ID_Object = configTo.ID_Other,
                    ID_RelationType = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_Files_Fields_Report_Field.ID_RelationType,
                    ID_Parent_Other = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_Files_Fields_Report_Field.ID_Class_Right
                }).ToList();

                var dbReaderFilesFields = new OntologyModDBConnector(globals);

                if (searchFilesFields.Any())
                {
                    result.ResultState = dbReaderFilesFields.GetDataObjectRel(searchFilesFields);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the File-Fields!";
                        return result;
                    }

                    result.Result.FilesFields = dbReaderFilesFields.ObjectRels;
                }

                var searchReportFilters = configsToExportReportConfigs.Select(configTo => new clsObjectRel
                {
                    ID_Object = configTo.ID_Other,
                    ID_RelationType = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_belonging_Report_Filter.ID_RelationType,
                    ID_Parent_Other = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_belonging_Report_Filter.ID_Class_Right
                }).ToList();


                var dbReaderReportFilters = new OntologyModDBConnector(globals);

                if (searchReportFilters.Any())
                {
                    result.ResultState = dbReaderReportFilters.GetDataObjectRel(searchReportFilters);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Filters!";
                        return result;
                    }

                    result.Result.ReportFilters = dbReaderReportFilters.ObjectRels;
                }

                var searchReports = configsToExportReportConfigs.Select(configTo => new clsObjectRel
                {
                    ID_Object = configTo.ID_Other,
                    ID_RelationType = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_belonging_Reports.ID_RelationType,
                    ID_Parent_Other = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_belonging_Reports.ID_Class_Right
                }).ToList();


                var dbReaderReports = new OntologyModDBConnector(globals);

                if (searchReports.Any())
                {
                    result.ResultState = dbReaderReports.GetDataObjectRel(searchReports);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Reports!";
                        return result;
                    }

                    result.Result.Reports = dbReaderReports.ObjectRels;
                }

                result.ResultState = ValidationController.ValidateGetExportReportModel(result.Result, globals, nameof(ExportReportModel.Reports));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchReportSorts = configsToExportReportConfigs.Select(configTo => new clsObjectRel
                {
                    ID_Object = configTo.ID_Other,
                    ID_RelationType = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_belonging_Report_Sort.ID_RelationType,
                    ID_Parent_Other = Export.Report.Config.LocalData.ClassRel_Export_Report_Json_belonging_Report_Sort.ID_Class_Right
                }).ToList();


                var dbReaderReportSorts = new OntologyModDBConnector(globals);

                if (searchReportSorts.Any())
                {
                    result.ResultState = dbReaderReportSorts.GetDataObjectRel(searchReportSorts);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Sorts!";
                        return result;
                    }

                    result.Result.ReportSorts = dbReaderReportSorts.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultClassPath> GetClassPath(clsOntologyItem objectItem)
        {
            var taskResult = await Task.Run<ResultClassPath>(() =>
            {
                var result = new ResultClassPath
                {
                    Result = globals.LState_Success.Clone(),
                    Path = ""
                };
                var dbReader = new OntologyModDBConnector(globals);
                var resultRead = dbReader.GetDataClasses(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = objectItem.GUID_Parent
                }
            });

                if (resultRead.GUID == globals.LState_Error.GUID)
                {

                    result.Result = resultRead;
                    ResultClassPath = result;
                    return result;
                }

                var classItem = dbReader.Classes1.FirstOrDefault();
                if (classItem == null)
                {
                    result.Result = globals.LState_Error.Clone();
                    ResultClassPath = result;
                    return result;
                }

                while (classItem.GUID != globals.Root.GUID)
                {
                    result.Path = $"{ classItem.Name }\\{result.Path}";

                    resultRead = dbReader.GetDataClasses(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = classItem.GUID_Parent
                    }
                });

                    if (resultRead.GUID == globals.LState_Error.GUID)
                    {

                        result.Result = resultRead;
                        ResultClassPath = result;
                        return result;
                    }

                    classItem = dbReader.Classes1.FirstOrDefault();
                }

                result.Path = $"{ globals.Root.Name }\\{result.Path}";

                return result;
            });

            return taskResult;
        }

        public async Task<ResultHtmlObject> GetHtmlDocumentByRef(clsOntologyItem oItemRef)
        {
            var taskResult = await Task.Run<ResultHtmlObject>(() =>
            {
                if (oItemRef.GUID_Parent == Config.LocalData.Class_HTML_Document.GUID)
                {
                    var result = new ResultHtmlObject
                    {
                        Result = globals.LState_Success.Clone(),
                        OItemHtmlItem = oItemRef
                    };

                    return result;
                }
                else
                {
                    var dbReader = new OntologyModDBConnector(globals);
                    var searchHtmlItem = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = oItemRef.GUID,
                            ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType,
                            ID_Parent_Object = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_Class_Left
                        }
                    };

                    var result = new ResultHtmlObject
                    {
                        Result = dbReader.GetDataObjectRel(searchHtmlItem)

                    };

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultHtmlObject = result;
                        return result;
                    }

                    var htmlItem = dbReader.ObjectRels.Select(relItm => new clsOntologyItem
                    {
                        GUID = relItm.ID_Object,
                        Name = relItm.Name_Object,
                        GUID_Parent = relItm.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).FirstOrDefault();

                    result.OItemHtmlItem = htmlItem;

                    ResultHtmlObject = result;

                    return result;
                }


            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteHistoryItems(List<string> historyItemIdList)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
              {
                  var result = globals.LState_Success.Clone();

                  var dbReader = new clsUserAppDBSelector(globals.Server,
                                                        globals.Port,
                                                        "htmleditorcontroller",
                                                        globals.SearchRange,
                                                        globals.Session);

                  var dbDeletor = new clsUserAppDBDeletor(dbReader);

                  result = dbDeletor.DeleteItemsById("htmleditorcontroller", "code", historyItemIdList);

                  return result;
              });

            return taskResult;

        }
        public async Task<clsOntologyItem> SaveDayStats(StatWorkerItem statWorkerItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                statWorkerItem.IdStatWorker = globals.NewGUID;
                statWorkerItem.NameStatWorker = $"{statWorkerItem.Start.ToString()} - {statWorkerItem.Ende.ToString()}";

                var objectsToSave = new List<clsOntologyItem>();
                var attributesToSave = new List<clsObjectAtt>();
                var relationsToSave = new List<clsObjectRel>();

                var statWorkOItem = new clsOntologyItem
                {
                    GUID = statWorkerItem.IdStatWorker,
                    Name = statWorkerItem.NameStatWorker,
                    GUID_Parent = Stat.Day.LocalData.Class_Stat_Worker.GUID,
                    Type = globals.Type_Object
                };

                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(statWorkOItem, Stat.Day.LocalData.AttributeType_Start, statWorkerItem.Start));
                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(statWorkOItem, Stat.Day.LocalData.AttributeType_Ende, statWorkerItem.Ende));

                objectsToSave.Add(statWorkOItem);

                foreach (var dayStat in statWorkerItem.DayStats)
                {
                    dayStat.IdReferencesDayStat = globals.NewGUID;
                    dayStat.NameReferencesDayStat = $"{dayStat.NameRefItem} ({dayStat.FirstEdit.Value.Date})";

                    var dayStatItem = new clsOntologyItem
                    {
                        GUID = dayStat.IdReferencesDayStat,
                        Name = dayStat.NameReferencesDayStat,
                        GUID_Parent = Stat.Day.LocalData.Class_Day_Stat_Item__HtmlEntries_.GUID,
                        Type = globals.Type_Object
                    };
                    objectsToSave.Add(dayStatItem);

                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(dayStatItem, Stat.Day.LocalData.AttributeType_Day, dayStat.Day));
                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(dayStatItem, Stat.Day.LocalData.AttributeType_First_Edit, dayStat.FirstEdit));
                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(dayStatItem, Stat.Day.LocalData.AttributeType_Last_Edit, dayStat.LastEdit));
                    attributesToSave.Add(relationConfig.Rel_ObjectAttribute(dayStatItem, Stat.Day.LocalData.AttributeType_Save_Count, dayStat.SaveCount));

                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(dayStatItem, new clsOntologyItem
                    {
                        GUID = dayStat.IdRefItem,
                        Name = dayStat.NameRefItem,
                        GUID_Parent = dayStat.IdRefParentItem,
                        Type = dayStat.TypeRefItem
                    }, Stat.Day.LocalData.RelationType_belongs_to));
                }

                var dbWriter = new OntologyModDBConnector(globals);

                result = dbWriter.SaveObjects(objectsToSave);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Stat-Worker log";
                    return result;
                }

                relationsToSave.AddRange(objectsToSave.Where(obj => obj.GUID_Parent != Stat.Day.LocalData.Class_Stat_Worker.GUID).Select(obj => relationConfig.Rel_ObjectRelation(statWorkOItem, obj, Stat.Day.LocalData.RelationType_contains)));

                if (relationsToSave.Any())
                {
                    result = dbWriter.SaveObjRel(relationsToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Stat-Worker log";
                        return result;
                    }
                }

                result = dbWriter.SaveObjAtt(attributesToSave);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the attributes!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<DateTime?>> GetLastHtmlStatLog()
        {
            var taskResult = await Task.Run<ResultItem<DateTime?>>(() =>
            {
                var result = new ResultItem<DateTime?>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchEnds = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_AttributeType = Stat.Day.LocalData.ClassAtt_Stat_Worker_Ende.ID_AttributeType,
                        ID_Class = Stat.Day.LocalData.ClassAtt_Stat_Worker_Ende.ID_Class
                    }
                };

                var dbReaderHtmlStatLog = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderHtmlStatLog.GetDataObjectAtt(searchEnds);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the End-Dates!";
                    return result;
                }

                if (dbReaderHtmlStatLog.ObjAtts.Any())
                {
                    result.Result = dbReaderHtmlStatLog.ObjAtts.Max(att => att.Val_Date.Value);
                }
                else
                {
                    var dbReaderFirstEntry = new clsUserAppDBSelector(globals.Server,
                                                       globals.Port,
                                                       "htmleditorcontroller",
                                                       globals.SearchRange,
                                                       globals.Session);
                    var sortFields = new List<Nest.SortField>
                    {
                        new Nest.SortField
                        {
                            Field = new Nest.Field("datestamp",null),
                            Order = Nest.SortOrder.Ascending
                        }
                    };
                    var docs = dbReaderFirstEntry.GetData_Documents(1, "htmleditorcontroller", "code", "*", page: 0);
                    if (docs.Documents.Any())
                    {
                        result.Result = (DateTime)docs.Documents.First().Dict["datestamp"];
                    }
                    else
                    {
                        result.Result = DateTime.Now.AddDays(-1);
                    }
                }


                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<HtmlHistoryEntriesRaw>> GetHtmlHistoryEntries(GetHtmlHistoriesByDateRangeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<HtmlHistoryEntriesRaw>>(() =>
           {
               var result = new ResultItem<HtmlHistoryEntriesRaw>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new HtmlHistoryEntriesRaw()
               };

               var query = "*";

               var startString = request.Start != null ? request.Start.Value.ToString("yyyy-MM-ddTHH:mm:ss") : "*";
               var endString = request.End != null ? request.End.Value.ToString("yyyy-MM-ddTHH:mm:ss") : "*";

               if (request.Start != null || request.End != null)
               {
                   query = $"datestamp:[{startString} TO {endString}]";
               }

               var dbReader = new clsUserAppDBSelector(globals.Server,
                                                        globals.Port,
                                                        "htmleditorcontroller",
                                                        globals.SearchRange,
                                                        globals.Session);

               var resultGetDocs = dbReader.GetData_Documents(5000, "htmleditorcontroller", "code", query);

               if (!resultGetDocs.IsOK)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = resultGetDocs.ErrorMessage;
                   return result;
               }

               result.Result.HtmlHistoryEntries = resultGetDocs.Documents;

               var searchObjects = result.Result.HtmlHistoryEntries.Select(dict => dict.Dict["ID_Object"]).GroupBy(idObject => idObject).Select(idObject => new clsOntologyItem { GUID = idObject.Key.ToString() }).ToList();

               var dbReaderObjects = new OntologyModDBConnector(globals);

               if (searchObjects.Any())
               {
                   result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Objects of HtmlHistory-Entries!";
                       return result;
                   }
               }

               result.Result.ObjectItems = dbReaderObjects.Objects1;

               var searchRelatedItems = result.Result.ObjectItems.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType
               }).ToList();

               var dbReaderReferences = new OntologyModDBConnector(globals);

               if (searchRelatedItems.Any())
               {
                   result.ResultState = dbReaderReferences.GetDataObjectRel(searchRelatedItems);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the references!";
                       return result;
                   }
               }


               result.Result.RefItems = dbReaderReferences.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsAppDocuments>>> GetHtmlHistoryEntries(clsOntologyItem oItemHtml)
        {
            var taskResult = await Task.Run<ResultItem<List<clsAppDocuments>>>(() =>
            {
                var result = new ResultItem<List<clsAppDocuments>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsAppDocuments>()
                };

                var dbReader = new clsUserAppDBSelector(globals.Server,
                                                        globals.Port,
                                                        "htmleditorcontroller",
                                                        globals.SearchRange,
                                                        globals.Session);

                var resultGetDocs = dbReader.GetData_Documents(5000, "htmleditorcontroller", "code", $"ID_Object:{oItemHtml.GUID}");


                if (!resultGetDocs.IsOK)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    return result;
                }

                result.Result = resultGetDocs.Documents;


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ResultHtmlSearch>> GetHtmlSearchResult(string searchString)
        {
            var taskResult = await Task.Run<ResultItem<ResultHtmlSearch>>(() =>
            {
                var result = new ResultItem<ResultHtmlSearch>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ResultHtmlSearch()
                };

                var searchHtmlCode = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Class = Config.LocalData.Class_HTML_Document.GUID,
                        ID_AttributeType = Config.LocalData.AttributeType_HTML.GUID,
                        Val_String = searchString
                    },
                    new clsObjectAtt
                    {
                        ID_Class = WordPress_Module.WordPressOntologyEngine.PostItem.GUID,
                        ID_AttributeType = WordPress_Module.WordPressOntologyEngine.AttributeTypeContent.GUID,
                        Val_String = searchString
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectAtt(searchHtmlCode);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Html-Code!";
                    return result;
                }

                result.Result.FoundHtmlCode = dbReader.ObjAtts.Where(att => att.Val_String.ToLower().Contains(searchString.ToLower())).ToList();

                var searchReference = result.Result.FoundHtmlCode.Select(html => new clsObjectRel
                {
                    ID_Object = html.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_HTML_Document_reference_to.ID_RelationType
                }).ToList();

                var dbReaderReference = new OntologyModDBConnector(globals);

                if (searchReference.Any())
                {
                    result.ResultState = dbReaderReference.GetDataObjectRel(searchReference);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the reference of Html-Object";
                        return result;
                    }
                    result.Result.FoundHtmlReferences = dbReaderReference.ObjectRels;
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetHtmlTag(clsOntologyItem documentType, long? orderId = null)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchHtmlTag = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Other = documentType.GUID,
                            ID_RelationType = Export.Config.LocalData.RelationType_belongs_to.GUID,
                            ID_Parent_Object = Export.Config.LocalData.Class_HTML_Tags.GUID
                        }
                };

                var dbReaderTag = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTag.GetDataObjectRel(searchHtmlTag);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the related Html-Tag";
                    return result;
                }

                List<clsOntologyItem> htmlTags;

                if (orderId == null)
                {
                    htmlTags = dbReaderTag.ObjectRels.Select(ht => new clsOntologyItem
                    {
                        GUID = ht.ID_Object,
                        Name = ht.Name_Object,
                        GUID_Parent = ht.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).ToList();
                }
                else
                {
                    htmlTags = dbReaderTag.ObjectRels.Where(ht => ht.OrderID == orderId).Select(ht => new clsOntologyItem
                    {
                        GUID = ht.ID_Object,
                        Name = ht.Name_Object,
                        GUID_Parent = ht.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).ToList();
                }

                result.Result = htmlTags.FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public HtmlEditAgent_Elastic(Globals globals) : base(globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            dbReader_HtmlDoc = new OntologyModDBConnector(globals);
            dbReader_HtmlCode = new OntologyModDBConnector(globals);
            dbReader_HtmlStyle = new OntologyModDBConnector(globals);
            relationConfig = new clsRelationConfig(globals);
            transaction = new clsTransaction(globals);
        }
    }


    public class ResultGetHtmlDocuments
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> HtmlDocumentsToRef { get; set; }
    }

    public class ResultHtmlDoc
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem HtmlDocument { get; set; }
        public clsObjectAtt HtmlOAttribute { get; set; }
        public List<clsObjectAtt> StylesOAttribute { get; set; } = new List<clsObjectAtt>();
    }

    public class ResultHtmlTemplates
    {
        public List<clsObjectRel> ClassToTemplates { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> TemplatesToHtmlDocuments { get; set; } = new List<clsObjectRel>();
    }

    public class ResultHtmlSearch
    {
        public string SearchString { get; set; }
        public List<clsObjectAtt> FoundHtmlCode { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> FoundHtmlReferences { get; set; } = new List<clsObjectRel>();
    }
}
