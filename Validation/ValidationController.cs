﻿using HtmlEditorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateGetExportAnonymousHtmlModel(GetModelRawResult modelResult, Globals globals, string propertyName = null )
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.RootConfig))
            {
                if (modelResult.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Root-Config cannot be found!";
                    return result;
                }
            }


            if (string.IsNullOrEmpty(propertyName) ||propertyName == nameof(modelResult.IndexCommandLineRuns))
            {
              
                if (modelResult.IndexCommandLineRuns.Count != modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Not all configs have a ComandLineRun!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.Paths))
            {
                
                if (modelResult.Paths.Count != modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Not all configs have a Path!";
                    return result;
                }

            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.Urls))
            {
                
                if (modelResult.Urls.Count != modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Not all configs have a Url!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.HTMLExportPacks))
            {
                
                if (modelResult.HTMLExportPacks.Count != modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Not all configs have a Export-Pack!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.AnonymousItems))
            {
                if (modelResult.AnonymousItems.Count != modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Not all configs have a Anonymous document!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateGetExportReportModel(ExportReportModel modelResult, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.JsonRowTemplates))
            {
                if (modelResult.JsonRowTemplates.Count != modelResult.ConfigsToExportReportsConfig.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need as much Row-Templates as Export-Configs!";
                    return result;
                }
            }


            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.JsonTableTemplates))
            {

                if (modelResult.JsonTableTemplates.Count != modelResult.ConfigsToExportReportsConfig.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need as much Table-Templates as Export-Configs!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(modelResult.Reports))
            {

                if (modelResult.Reports.Count != modelResult.ConfigsToExportReportsConfig.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need as much Reports as Export-Configs!";
                    return result;
                }

            }

            return result;
        }

        public static clsOntologyItem ValidateHTMLDiffRequest(HTMLDiffRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!request.HtmlDiffItems.Any())
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No HTMLDiff-Items present!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetRankedFoundWordsByRefAndHtmlRequest(GetRankedFoundWordsByRefAndHtmlRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request.IdRefItem == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdRefItem is empty!";
                return result;
            }
            else if (!globals.is_GUID(request.IdRefItem))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdRefItem is no valid id!";
                return result;
            }

            return result;
        }
    }
}
