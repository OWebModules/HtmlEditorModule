﻿using HtmlEditorModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HtmlEditorModule.Converters
{
    public static class HtmlToPlainTextConverter
    {
        public static string Convert(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            var htmlNameMapList = new List<HTMLNameMap>();

            htmlNameMapList.Add(new HTMLNameMap("&Auml;", "&#196;", "Ä"));
            htmlNameMapList.Add(new HTMLNameMap("&auml;", "&#228;", "ä"));
            htmlNameMapList.Add(new HTMLNameMap("&Euml;", "&#203;", "Ë"));
            htmlNameMapList.Add(new HTMLNameMap("&euml;", "&#235;", "ë"));
            htmlNameMapList.Add(new HTMLNameMap("&Iuml;", "&#207;", "Ï"));
            htmlNameMapList.Add(new HTMLNameMap("&iuml;", "&#239;", "ï"));
            htmlNameMapList.Add(new HTMLNameMap("&Ouml;", "&#214;", "Ö"));
            htmlNameMapList.Add(new HTMLNameMap("&ouml;", "&#246;", "ö"));
            htmlNameMapList.Add(new HTMLNameMap("&Uuml;", "&#220;", "Ü"));
            htmlNameMapList.Add(new HTMLNameMap("&uuml;", "&#252;", "ü"));
            htmlNameMapList.Add(new HTMLNameMap("&szlig;", "&#223;", "ß"));
            htmlNameMapList.Add(new HTMLNameMap("&Agrave;", "&#192;", "À"));
            htmlNameMapList.Add(new HTMLNameMap("&agrave;", "&#224;", "à"));
            htmlNameMapList.Add(new HTMLNameMap("&Aacute;", "&#193;", "Á"));
            htmlNameMapList.Add(new HTMLNameMap("&aacute;", "&#225;", "á"));
            htmlNameMapList.Add(new HTMLNameMap("&Acirc;", "&#194;", "Â"));
            htmlNameMapList.Add(new HTMLNameMap("&acirc;", "&#226;", "â"));
            htmlNameMapList.Add(new HTMLNameMap("&Ccedil;", "&#199;", "Ç"));
            htmlNameMapList.Add(new HTMLNameMap("&ccedil;", "&#231;", "ç"));
            htmlNameMapList.Add(new HTMLNameMap("&Egrave;", "&#200;", "È"));
            htmlNameMapList.Add(new HTMLNameMap("&egrave;", "&#232;", "è"));
            htmlNameMapList.Add(new HTMLNameMap("&Eacute;", "&#201;", "É"));
            htmlNameMapList.Add(new HTMLNameMap("&eacute;", "&#233;", "é"));
            htmlNameMapList.Add(new HTMLNameMap("&Ecirc;", "&#202;", "Ê"));
            htmlNameMapList.Add(new HTMLNameMap("&ecirc;", "&#234;", "ê"));
            htmlNameMapList.Add(new HTMLNameMap("&Ntilde;", "&#209;", "Ñ"));
            htmlNameMapList.Add(new HTMLNameMap("&ntilde;", "&#241;", "ñ"));
            htmlNameMapList.Add(new HTMLNameMap("&Ograve;", "&#210;", "Ò"));
            htmlNameMapList.Add(new HTMLNameMap("&ograve;", "&#242;", "ò"));
            htmlNameMapList.Add(new HTMLNameMap("&Oacute;", "&#211;", "Ó"));
            htmlNameMapList.Add(new HTMLNameMap("&oacute;", "&#243;", "ó"));
            htmlNameMapList.Add(new HTMLNameMap("&Ocirc;", "&#212;", "Ô"));
            htmlNameMapList.Add(new HTMLNameMap("&ocirc;", "&#244;", "ô"));
            htmlNameMapList.Add(new HTMLNameMap("&otilde;", "&#245;", "õ"));
            htmlNameMapList.Add(new HTMLNameMap("&Yuml;", "&#195;", "Ÿ"));
            htmlNameMapList.Add(new HTMLNameMap("&yuml;", "&#255;", "ÿ"));

            htmlNameMapList.ForEach(map =>
            {
                text = map.Replace(text);
            });

            return text;
        }
    }
}
