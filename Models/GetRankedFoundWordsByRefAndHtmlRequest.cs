﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class GetRankedFoundWordsByRefAndHtmlRequest
    {
        public string IdRefItem { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetRankedFoundWordsByRefAndHtmlRequest(string idRefItem)
        {
            IdRefItem = idRefItem;
        }
    }
}
