﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class StatWorkerItem
    {
        public string IdStatWorker { get; set; }
        public string NameStatWorker { get; set; }
        public DateTime Start { get; set; }
        public DateTime Ende { get; set; }

        public List<ReferencesDayStat> DayStats { get; set; } = new List<ReferencesDayStat>();

    }
}
