﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class SearchEntry
    {
        public string id { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }

        public SearchEntry(string id, string text, string title, string fileName)
        {
            this.id = id;
            Text = text;
            Title = title;
            FileName = fileName;
        }
    }
}
