﻿using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class KendoTreeData
    {
        public KendoHierarchicalDataSource dataSource { get; set; }
        public List<KendoTreeNode> data { get; set; } = new List<KendoTreeNode>();
    }
}
