﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class HTMLDiffRequest
    {
        public List<HTMLDiffItem> HtmlDiffItems { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public HTMLDiffRequest(List<HTMLDiffItem> htmlDiffItems)
        {
            HtmlDiffItems = htmlDiffItems;
        }
    }

    public class HTMLDiffItem
    {
        public string IdReference { get; set; }
        public string Html1 { get; set; }
        public string Html2 { get; set; }
        public string HtmlDiff { get; set; }
    }
}
