﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class LinkItem
    {
        public string Url { get; set; }
        public string NameLink { get; set; }
        public string IdObject { get; set; }
        public string NameObject { get; set; }

        public string EncodedNameObject { get; set; }

        public bool IsTypedTag { get; set; }
        public bool IsImage { get; set; }
        public bool IsAudio { get; set; }
        public bool IsVideo { get; set; }
        public bool IsPDF { get; set; }
    }
}
