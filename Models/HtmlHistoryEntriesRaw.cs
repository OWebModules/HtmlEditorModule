﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class HtmlHistoryEntriesRaw
    {
        public List<clsOntologyItem> ObjectItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> RefItems { get; set; } = new List<clsObjectRel>();

        public List<clsAppDocuments> HtmlHistoryEntries { get; set; } = new List<clsAppDocuments>();
    }
}
