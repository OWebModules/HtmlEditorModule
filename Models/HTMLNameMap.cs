﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class HTMLNameMap
    {
        public string HtmlName { get; set; }
        public string Unicode { get; set; }

        public string Character { get; set; }

        public string Replace(string text)
        {
            text = text.Replace(HtmlName, Character);
            text = text.Replace(Unicode, Character);

            return text;
        }

        public HTMLNameMap(string htmlName, string unicode, string character)
        {
            HtmlName = htmlName;
            Unicode = unicode;
            Character = character;
        }
    }
}
