﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class ExportAnonymousHtmlRequest
    {
        public string IdConfig { get; set; }

        public bool ExportHtmlPack { get; set; } = true;
        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public ExportAnonymousHtmlRequest(string idConfig)
        {
            IdConfig = idConfig;
        }

    }
}
