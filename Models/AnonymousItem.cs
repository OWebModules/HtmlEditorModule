﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace HtmlEditorModule.Models
{
    public class AnonymousItem
    {
        public clsOntologyItem ConfigItem { get; set; }
        public clsOntologyItem AnonymousAllowed { get; set; }
        public List<clsOntologyItem> RefItems { get; set; } = new List<clsOntologyItem>();
        public List<Reference> TypedReferences { get; set; } = new List<Reference>();
    }
}
