﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class GetHtmlHistoryByDateEntriesResult
    {
        public List<HtmlHistoryItem> HtmlHistoryEntries { get; set; } = new List<HtmlHistoryItem>();
    }
}
