﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class GetModelRawResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> StatsToConfigs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> StatsDateTime { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> ExportLastEdit { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> Paths { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Urls { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> HTMLExportPacks { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> IndexCommandLineRuns { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ForDocsCommandLineRuns { get; set; } = new List<clsObjectRel>();
        public List<AnonymousItem> AnonymousItems { get; set; } = new List<AnonymousItem>();
        public List<clsObjectRel> ExportReportItems { get; set; } = new List<clsObjectRel>();

    }
}
