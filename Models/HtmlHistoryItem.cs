﻿using OntologyClasses.BaseClasses;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.multipleRow,
        editable = EditableType.False,
        height = "100%")]
    [KendoFilter(mode = "row")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoNumberFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class HtmlHistoryItem
    {
        [KendoColumn(hidden = true)]
        public string IdEsDoc { get; set; }
        [KendoColumn(hidden = true)]
        public string IdHtmlDoc { get; set; }
        
        public string NameHtmlDoc { get; set; }
        [KendoColumn(hidden = true)]
        public string IdRef { get; set; }
        public string NameRef { get; set; }
        [KendoColumn(hidden = true)]
        public string IdRefParent { get; set; }

        [KendoColumn(hidden = true)]
        public string TypeParent { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Datum", template = "#= HistoryStamp != null ? kendo.toString(kendo.parseDate(HistoryStamp), 'dd.MM.yyyy HH:mm:ss') : '' #", type = ColType.DateType)]
        public DateTime HistoryStamp { get; set; }

    }

    public class HtmlHistoryItemWithRaw
    {
        public HtmlHistoryItem HistoryItem { get; set; }
        public clsAppDocuments EsDoc { get; set; }
    }
}
