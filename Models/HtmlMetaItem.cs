﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace HtmlEditorModule.Models
{
    public class HtmlMetaItem
    {
        public string IdRef { get; set; }
        public string NameRef { get; set; }
        public List<Reference> References { get; set; } = new List<Reference>();
    }
}
