﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class ReferencesDayStat
    {
        public string IdReferencesDayStat { get; set; }
        public string NameReferencesDayStat { get; set; }

        public DateTime Day { get; set; }
        public DateTime? FirstEdit { get; set; }
        public DateTime? LastEdit { get; set; }
        public long SaveCount { get; set; }
        public string IdRefItem { get; set; }
        public string NameRefItem { get; set; }
        public string IdRefParentItem { get; set; }
        public string TypeRefItem { get; set; }
    }
}
