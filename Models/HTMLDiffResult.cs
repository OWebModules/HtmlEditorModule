﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class HTMLDiffResult
    {
        public List<ResultItem<HTMLDiffItem>> HTMLDiffItems { get; set; } = new List<ResultItem<HTMLDiffItem>>();
    }
}
