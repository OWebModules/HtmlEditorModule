﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class ExportReportModel
    {
        public List<clsObjectRel> ConfigsToExportReportsConfig { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> JsonRowTemplates { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> JsonTableTemplates { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> FilesFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ReportFilters { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> Reports { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ReportSorts { get; set; } = new List<clsObjectRel>();
    }
}
