﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class GetHtmlTagRequest
    {
        public Dictionary<string, string> Attributes { get; set; } = new Dictionary<string, string>();
        public clsOntologyItem DocumentType { get; set; }
        public bool Finalize { get; set; } 

        public long? OrderId { get; set; }

        public GetHtmlTagRequest(clsOntologyItem documentType, bool finalize)
        {
            DocumentType = documentType;
            Finalize = finalize;
        }
    }
}
