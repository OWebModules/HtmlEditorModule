﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class GetModelHtmlExportResult
    {
        public clsOntologyItem BaseConfig { get; set; }
        public clsOntologyItem TagStart { get; set; }
        public clsOntologyItem TagEnd { get; set; }
        public clsOntologyItem TagEndInit { get; set; }


    }
}
