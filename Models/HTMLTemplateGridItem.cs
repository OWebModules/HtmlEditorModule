﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class HTMLTemplateGridItem
    {
        [KendoColumn(hidden = true)]
        public string IdTemplate { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Name")]
        public string NameTemplate { get; set; }

        private bool viewTemplate;
        [KendoColumn(hidden = false, Order = 1, filterable = false, title = "Show", template = "<button class='show-template-button'>Show</button>")]
        public bool ViewTemplate
        {
            get { return viewTemplate; }
            set
            {
                if (viewTemplate == value) return;

                viewTemplate = value;

            }
        }

        private bool getTemplate;
        [KendoColumn(hidden = false, Order = 2, filterable = false, title = "Use", template = "<button class='use-template-button'>Use</button>")]
        public bool GetTemplate
        {
            get { return getTemplate; }
            set
            {
                if (getTemplate == value) return;

                getTemplate = value;

            }
        }
    }


}
