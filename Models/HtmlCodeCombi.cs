﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class HtmlCodeCombi
    {
        public string IdHtmlDoc { get; set; }

        public string IdRef { get; set; }
        public string IdHistoryEntryDoc { get; set; }
        public string EncodedHistoryHtml { get; set; }
        public string EncodedHtmlDoc { get; set; }
        
    }
}
