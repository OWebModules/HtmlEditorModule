﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class ReferencedHtmlCode
    {
        public List<string> ReferenceIds { get; set; } = new List<string>();
        public string HtmlCode { get; set; }
    }
}
