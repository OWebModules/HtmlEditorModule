﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class GetHtmlHistoriesByDateRangeRequest
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }

        public IMessageOutput MessageOutput { get; set; }
    }
}
