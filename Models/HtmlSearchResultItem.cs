﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    [KendoGridConfig(
       groupbable = true,
       autoBind = false,
       scrollable = true,
       resizable = true,
       selectable = SelectableType.row,
       editable = EditableType.False,
       height = "100%")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class HtmlSearchResultItem
    {

        [KendoColumn(hidden = true)]
        public string IdHtmlDocument { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Document")]
        public string NameHtmlDocument { get; set; }

        [KendoColumn(hidden = true)]
        public string IdDocumentClass { get; set; }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Class")]
        public string NameDocumentClass { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReference { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Reference")]
        public string NameReference { get; set; }

        [KendoColumn(hidden = true)]
        public string IdParentReference { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Reference-Class")]
        public string NameParentReference { get; set; }

        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Code-Snipplet")]
        public string CodeSnipplet { get; set; }
    }
}
