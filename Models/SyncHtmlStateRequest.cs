﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorModule.Models
{
    public class SyncHtmlStateRequest
    {
        public DateTime? Start { get; set; }
        public IMessageOutput MessageOutput { get; set; }
    }
}
