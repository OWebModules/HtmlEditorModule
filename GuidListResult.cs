﻿using OntologyClasses.BaseClasses;
using System.Collections.Generic;

namespace HtmlEditorModule
{
    public class GuidListResult
    {
        public clsOntologyItem Result { get; set; }
        public List<string> GuidList { get; set; }
    }
}
