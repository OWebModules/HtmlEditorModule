﻿using HtmlEditorModule.Factories;
using HtmlEditorModule.Models;
using HtmlEditorModule.Services;
using HtmlEditorModule.Validation;
using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;

namespace HtmlEditorModule
{
    public class HtmlEditorConnector : AppController
    {
        private HtmlEditAgent_Elastic serviceElastic;
        private TypedTaggingConnector typedTaggingConnector;
        private object connectorLocker = new object();

        private clsOntologyItem oItem;
        public clsOntologyItem OItem
        {
            get
            {
                lock (connectorLocker)
                {
                    return oItem;
                }
            }
            set
            {
                lock (connectorLocker)
                {
                    oItem = value;
                }
            }
        }

        public clsOntologyItem ClassHtmlDocument
        {
            get { return Config.LocalData.Class_HTML_Document; }
        }

        private ResultHtmlObject resultHtmlObject;
        public ResultHtmlObject ResultHtmlObject
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultHtmlObject;
                }
            }
            set
            {
                lock (connectorLocker)
                {
                    resultHtmlObject = value;
                }
            }
        }

        private ResultTypedTags resultTypedTags;
        public ResultTypedTags ResultTypedTags
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultTypedTags;
                }
            }
            set
            {
                lock (connectorLocker)
                {
                    resultTypedTags = value;
                }
            }
        }

        public HtmlEditorConnector(Globals Globals) : base(Globals)
        {
            Initialize();
        }

        public async Task<ResultGetHtmlDocuments> GetHtmlDocumentsByRefs(List<clsOntologyItem> refs)
        {

            var result = await serviceElastic.GetHtmlDocuments(refs);

            return result;
        }

        public async Task<ConnectorResultHtmlDocument> GetHtmlDocumentForViewer(string viewerHtmlUrl, string idRef)
        {
            var result = await GetHtmlDocument(idRef);
            if (result.Result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var regexChange = new Regex("href=\"(HtmlEditorJQ.html|../Modules/ModuleStarter.html).Class=" + Globals.RegexGuid + "(.|.{5})Object=" + Globals.RegexGuid + "\"");
            var regexObject = new Regex("(O|o)bject=" + Globals.RegexGuid);
            var regexObjectId = new Regex(Globals.RegexGuid);
            var contentBuilder = new StringBuilder();

            if (result.OAItemHtmlDocument != null && result.OAItemHtmlDocument.Val_String != null)
            {
                contentBuilder.AppendLine("<div id='title" + result.OAItemHtmlDocument.ID_Object + "' style='visibility:hidden'>" + result.OAItemHtmlDocument.Name_Object + "</div>");

            }

            var objectId = "";
            var text = result.OAItemHtmlDocument?.Val_String ?? "";
            var matches = regexChange.Matches(text);
            if (matches.Count > 0)
            {
                var start = 0;
                matches.Cast<Match>().ToList().ForEach(matchItem =>
                {
                    var index = matchItem.Index;
                    var htmlViewer = true;
                    if (!matchItem.Value.Contains("HtmlEditorJQ"))
                    {
                        htmlViewer = false;
                    }
                    var length = matchItem.Length;
                    matchItem = regexObject.Match(matchItem.Value);
                    if (matchItem.Success)
                    {
                        var objectString = matchItem.Value;
                        matchItem = regexObjectId.Match(objectString);
                        if (matchItem.Success)
                        {
                            objectId = matchItem.Value;
                        }

                        contentBuilder.Append(result.OAItemHtmlDocument.Val_String.Substring(start, index - start));
                        if (htmlViewer)
                        {
                            contentBuilder.Append("href=\"" + viewerHtmlUrl + "?Object=" + objectId + "\"");
                        }
                        else
                        {
                            contentBuilder.Append("href=\"/ModuleMgmt/ModuleStarter?Object=" + objectId + "\"");
                        }

                        start = index + length;

                    }

                });
                if (start < text.Length)
                {
                    contentBuilder.Append(result.OAItemHtmlDocument?.Val_String.Substring(start) ?? "");
                }
            }
            else
            {
                contentBuilder.Append(result.OAItemHtmlDocument?.Val_String ?? "");
            }

            if (result.OAItemHtmlDocument != null)
            {
                result.OAItemHtmlDocument.Val_String = contentBuilder.ToString();
            }

            return result;
        }

        public async Task<ConnectorResultHtmlDocument> GetHtmlDocument(string idRef)
        {

            var oItemRef = await serviceElastic.GetOItem(idRef, Globals.Type_Object);

            var result = new ConnectorResultHtmlDocument
            {
                Result = oItemRef.ResultState,
                OItemRef = oItemRef.Result
            };


            if (result.Result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var resultTask = await serviceElastic.GetHtmlDocument(oItemRef.Result);
            result.Result = resultTask.Result;
            result.OItemHtmlDocument = resultTask.HtmlDocument;
            result.OAItemHtmlDocument = resultTask.HtmlOAttribute;
            result.OAItemStyles = resultTask.StylesOAttribute;

            return result;

        }

        public async Task<ResultItem<List<ConnectorResultHtmlDocument>>> GetHtmlDocuments(List<string> idRefs)
        {

            var oItems = idRefs.Select(id => new clsOntologyItem
            {
                GUID = id,
            }).ToList();
            var oItemRefs = await serviceElastic.GetObjectItemsWithGuid(oItems);

            var result = new ResultItem<List<ConnectorResultHtmlDocument>>
            {
                ResultState = oItemRefs.ResultState
            };

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the HTML-Documents!";
                return result;
            }

            var resultTask = await serviceElastic.GetHtmlDocumentsFull(oItemRefs.Result);
            result.ResultState = resultTask.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {   
                return result;
            }

            result.Result = resultTask.Result.Select(res => new ConnectorResultHtmlDocument
            {
                Result = res.Result,
                OItemRef = res.RefItem,
                OItemHtmlDocument = res.HtmlDocument,
                OAItemHtmlDocument = res.HtmlOAttribute,
                OAItemStyles = res.StylesOAttribute
            }).ToList();

            return result;

        }

        public async Task<ResultItem<clsObjectAtt>> GetHtmlContent(clsOntologyItem oItemHtml)
        {
            var serviceResult = await serviceElastic.GetHtmlCode(oItemHtml);

            return serviceResult;
        }

        public async Task<clsOntologyItem> CheckTags(clsOntologyItem oItemHtmlDoc, clsOntologyItem refItem, IEnumerable<string> guids, clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            var result = Globals.LState_Success.Clone();



            var tags = await GetTags(oItemHtmlDoc.GUID, "", "");

            var tagItemsExists = tags.References.SelectMany(reference => reference.Tags);

            //var newGuids = (from guid in guids
            //                join reference in tags.References on guid equals reference.Id into references
            //                from reference in references.DefaultIfEmpty()
            //                where reference == null
            //                select guid);

            var typedTagsToRemove = (from typedTag in tagItemsExists
                                     join guid in guids on typedTag.IdTag equals guid into guidItems
                                     from guid in guidItems.DefaultIfEmpty()
                                     where string.IsNullOrEmpty(guid)
                                     select typedTag).Select(typedTag =>
                                     {
                                         var resultTypedTag = new clsOntologyItem
                                         {
                                             GUID = typedTag.IdTypedTag,
                                             Name = typedTag.NameTypedTag,
                                             Type = Globals.Type_Object
                                         };
                                         return resultTypedTag;
                                     }).ToList();

            var orderId = 1;
            var itemsToAddPre = await Task.WhenAll((from guid in guids
                                                    join typedTag in typedTaggingConnector.ResultFoundTypedTags.TypedTags on guid equals typedTag.IdTag into typedTags
                                                    from typedTag in typedTags.DefaultIfEmpty()
                                                    where typedTag == null
                                                    select guid).Select(async guid =>
                                                    {
                                                        var oItem = await GetOItem(guid, Globals.Type_Object);
                                                        if (oItem.ResultState.GUID == Globals.LState_Error.GUID)
                                                        {
                                                            return null;
                                                        }
                                                        oItem.Result.Val_Long = orderId;
                                                        return oItem.Result;
                                                    }).Where(itemToAdd => itemToAdd != null));

            var typedTags1 = typedTaggingConnector.ResultFoundTypedTags.TypedTags.Select(typedTag => new clsOntologyItem
            {
                GUID = typedTag.IdTypedTag,
                Name = typedTag.NameTypedTag,
                GUID_Parent = typedTaggingConnector.ClassTypedTag.GUID,
                Type = Globals.Type_Object,
                Val_Long = typedTag.OrderId
            }).ToList();

            var itemsToAdd = itemsToAddPre.Where(itm => itm != null);
            if (itemsToAdd.Any())
            {
                var resultSave = await SaveTags(oItemHtmlDoc, itemsToAdd.ToList(), userItem, groupItem);
                typedTags1.AddRange(resultSave.TypedTags.Select(typedTag => new clsOntologyItem
                {
                    GUID = typedTag.IdTypedTag,
                    Name = typedTag.NameTypedTag,
                    GUID_Parent = typedTaggingConnector.ClassTypedTag.GUID,
                    Type = Globals.Type_Object,
                    Val_Long = typedTag.OrderId
                }));
            }

            typedTags1 = (from typedTag in typedTags1
                          join typedTagToRemove in typedTagsToRemove on typedTag.GUID equals typedTagToRemove.GUID into typedTagsToRemove1
                          from typedTagToRemove in typedTagsToRemove1.DefaultIfEmpty()
                          where typedTagToRemove == null
                          select typedTag).ToList();

            result = await typedTaggingConnector.RelateTypedTags(refItem, typedTags1);

            if (result.GUID == Globals.LState_Error.GUID)
            {
                result.Additional1 = "Error while relating typed tags to reference!";
                return result;
            }

            foreach (var tagToRemove in typedTagsToRemove)
            {
                var resultTask1 = await typedTaggingConnector.DeleteTag(tagToRemove);
                if (resultTask1.GUID == Globals.LState_Error.GUID)
                {
                    result = Globals.LState_Error.Clone();
                    return result;
                }
            }




            if (!itemsToAdd.Any() && !typedTagsToRemove.Any())
            {

                var typedTagsToReorder = (from guid in guids
                                          join typedTag in typedTaggingConnector.ResultFoundTypedTags.TypedTags on guid equals typedTag.IdTag
                                          select typedTag).ToList();

                if (typedTagsToReorder.Any())
                {
                    var resultTask2 = await typedTaggingConnector.ReorderTypedTags(typedTagsToReorder);
                    result = resultTask2.Result;
                }


            }

            return result;
        }



        public async Task<ConnectorResultTags> GetTags(string idHtmlDocument, string baseUrl, string baseUrlModeleStarter)
        {
            var oItemHtmlDocResult = await serviceElastic.GetOItem(idHtmlDocument, Globals.Type_Object);

            var result = new ConnectorResultTags
            {
                Result = oItemHtmlDocResult.ResultState
            };

            if (result.Result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var tagsTask = await typedTaggingConnector.GetTags(oItemHtmlDocResult.Result);

            var tags = tagsTask.TypedTags.Where(typedTag => typedTag.IdTagParent != "902b2cd20fe64f4992fd97bcf28e2dad"
                && typedTag.IdTagParent != "f62cf1ccee51475f9c007094f1809b56"
                && typedTag.IdTagParent != "d84fa125dbce44b091539abeb66ad27f"
                && typedTag.IdTagParent != "d0f582137db44882bd15962163015d13"
                && typedTag.IdTagParent != "e8f6f1ef76b94d78a234d8be72c0fdf2").OrderBy(typedTag => typedTag.OrderId).ThenBy(typedTag => typedTag.NameTag);
            var otherRefTypes = tags.Where(typedTag => typedTag.IdTagParent != "ce29760f023043daa19d91c6d82a8437" && typedTag.IdTagParent != "3bb4b05d8fcb48b7a1ac0fb42a265c38").GroupBy(typedTag => new { Id = typedTag.IdTagParent, Name = typedTag.NameTagParent }).OrderBy(refGroup => refGroup.Key.Name).ToList();

            var htmlDocs = await GetHtmlDocumentsByRefs(tags.Select(tag => new clsOntologyItem { GUID = tag.IdTag }).ToList());


            var tagsWithReferences = (from tag in tags
                                      join htmlDoc in htmlDocs.HtmlDocumentsToRef on tag.IdTag equals htmlDoc.ID_Other into htmlDocs1
                                      from htmlDoc in htmlDocs1.DefaultIfEmpty()
                                      select new TypedTagWithHtmlPage(tag, tag.IdTagParent != "094d728d6efc463c85c72dcfed903c78" ? htmlDoc != null ? baseUrl + "/DocViewer?Object=" + htmlDoc.ID_Object : "" : tag.NameTag, baseUrlModeleStarter));

            var references = new List<Reference>();
            var sceneTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "ce29760f023043daa19d91c6d82a8437");

            if (sceneTags.Any())
            {
                references.Add(new Reference
                {
                    Id = "ce29760f023043daa19d91c6d82a8437",
                    Name = "Szenen",
                    Tags = sceneTags.ToList()
                });
            }

            var termTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "3bb4b05d8fcb48b7a1ac0fb42a265c38");

            if (termTags.Any())
            {
                references.Add(new Reference
                {
                    Id = "3bb4b05d8fcb48b7a1ac0fb42a265c38",
                    Name = "Schlagwörter",
                    Tags = termTags.OrderBy(term => term.NameTag).ToList()
                });
            }

            otherRefTypes.ForEach(refGroup =>
            {
                var reference = new Reference
                {
                    Id = refGroup.Key.Id,
                    Name = refGroup.Key.Name
                };

                reference.Tags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == refGroup.Key.Id).ToList();
                references.Add(reference);
            });

            result.References = references;
            //result.Result = tagsTask.Result;
            //result.TypedTags = tagsTask.TypedTags;

            //return result;
            return result;
        }


        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> ids)
        {
            var serviceElastic = new HtmlEditAgent_Elastic(Globals);
            return await serviceElastic.GetObjects(ids);
        }

        public async Task<ResultItem<List<LinkItem>>> GetLinkItemsByMediaListItems(List<MediaListItem> mediaListItems)
        {
            var factory = new Factories.LinkItemFactory();
            return await factory.CreateLinkItemList(mediaListItems, Globals);
        }

        public async Task<ResultItem<List<LinkItem>>> GetLinkItemsByTypedTags(string baseUrl, List<TypedTag> typedTags)
        {
            var factory = new Factories.LinkItemFactory();
            return await factory.CreateLinkItemList(baseUrl, typedTags, Globals);
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjectItemsWithGuid(List<clsOntologyItem> objectItems)
        {
            return await serviceElastic.GetObjectItemsWithGuid(objectItems);
        }

        public async Task<ResultHtmlObject> GetHtmlObjectByRef(clsOntologyItem oItemRef)
        {

            var result = await serviceElastic.GetHtmlDocumentByRef(oItemRef);

            ResultHtmlObject = result;
            return result;

        }



        public async Task<ResultGetHtmlHierarchy> GetHtmlHierarchy(string idRef)
        {
            var taskResult = await Task.Run<ResultGetHtmlHierarchy>(async () =>
            {
                var result = new ResultGetHtmlHierarchy
                {
                    Result = Globals.LState_Success.Clone()
                };

                var refItemResult = await serviceElastic.GetOItem(idRef, Globals.Type_Object);
                result.Result = refItemResult.ResultState;
                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var htmlResultTask = await serviceElastic.GetRelatedHtmlDocumentWithoutCode(refItemResult.Result);

                if (htmlResultTask.Result == null)
                {
                    result.Result = Globals.LState_Nothing.Clone();
                    return result;
                }

                var resultTask = GetHierarchy(htmlResultTask.Result.GUID, htmlResultTask.Result.Name, new List<string>());
                result = resultTask.Result;
                result.HtmlDocumentOfRef = htmlResultTask.Result;
                return result;
            });

            return taskResult;

        }

        public async Task<ConnectorResultTypedTags> GetTags(string idParent)
        {
            var taskResult = await Task.Run<ConnectorResultTypedTags>(async () =>
            {
                var oItemsResult = await serviceElastic.GetOItem(idParent, Globals.Type_Object);
                var result = new ConnectorResultTypedTags
                {
                    Result = oItemsResult.ResultState
                };

                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var tagsTask = await typedTaggingConnector.GetTags(oItemsResult.Result);

                return tagsTask;
            });

            return taskResult;

        }

        public async Task<ResultGetHtmlHierarchy> GetHierarchy(string idParent, string nameParent, List<string> hierarchyList)
        {
            var taskResult = await Task.Run<ResultGetHtmlHierarchy>(async () =>
            {
                var result = new ResultGetHtmlHierarchy
                {
                    Result = Globals.LState_Success.Clone()
                };

                var oItemsResult = await serviceElastic.GetOItem(idParent, Globals.Type_Object);
                result.Result = oItemsResult.ResultState;

                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var tagsTask = await typedTaggingConnector.GetTags(oItemsResult.Result);

                if (tagsTask.Result.GUID == Globals.LState_Error.GUID)
                {
                    result.Result = Globals.LState_Error.Clone();
                    return result;
                }

                var htmlDocuments = new List<clsObjectRel>();

                hierarchyList.Add(idParent);
                foreach (var typedTag in tagsTask.TypedTags.OrderBy(typedTag => typedTag.OrderId).Where(typedTag => typedTag.IdTagParent == Config.LocalData.Class_HTML_Document.GUID))
                {
                    if (!hierarchyList.Contains(typedTag.IdTag))
                    {
                        htmlDocuments.Add(new clsObjectRel
                        {
                            ID_Object = idParent,
                            Name_Object = nameParent,
                            ID_Parent_Object = Config.LocalData.Class_HTML_Document.GUID,
                            OrderID = typedTag.OrderId,
                            ID_Other = typedTag.IdTag,
                            Name_Other = typedTag.NameTag,
                            ID_Parent_Other = Config.LocalData.Class_HTML_Document.GUID
                        });

                        var resultSubTask = await GetHierarchy(typedTag.IdTag, typedTag.NameTag, hierarchyList);

                        if (resultSubTask.Result.GUID == Globals.LState_Error.GUID)
                        {
                            return resultSubTask;
                        }

                        htmlDocuments.AddRange(resultSubTask.Hierarchy);
                    }
                }


                result.Hierarchy = htmlDocuments;
                return result;
            });

            return taskResult;


        }



        public async Task<ResultItem<List<HTMLTemplateGridItem>>> GetHtmlTemplateByRef(clsOntologyItem oItemRef, List<clsOntologyItem> transformedItems)
        {
            var result = new ResultItem<List<HTMLTemplateGridItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<HTMLTemplateGridItem>()
            };

            var serviceResult = await serviceElastic.GetHtmlDocumentOfTemplate(oItemRef);
            result.ResultState = serviceResult.ResultState;
            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var factory = new HtmlTemplateFactory(Globals);
            var factoryResult = await factory.GetHtmlTemplateGridItems(serviceResult.Result, transformedItems);

            result.ResultState = factoryResult.ResultState;
            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = factoryResult.Result;

            return result;
        }

        public async Task<ResultClassPath> GetClassPath(clsOntologyItem objectItem)
        {

            var agentTask = await serviceElastic.GetClassPath(objectItem);
            return agentTask;



        }

        public async Task<GuidListResult> GetGuidList(clsObjectAtt oAttributeHtml)
        {
            var taskResult = await Task.Run<GuidListResult>(() =>
            {
                var regexGuid = new Regex("name=\"_?" + Globals.RegexGuid + "\"");
                var foundGuids = regexGuid.Matches(oAttributeHtml.Val_String);
                var result = new GuidListResult
                {
                    Result = Globals.LState_Success.Clone(),
                    GuidList = foundGuids.Cast<Match>().Select(match => match.Value.Replace("_", "").Replace("name=", "").Replace("\"", "")).Where(matchValue => Globals.is_GUID(matchValue)).GroupBy(guid => new { guid }).Select(guidItem => guidItem.Key.guid).ToList()
                };
                return result;
            });

            return taskResult;
        }

        public async Task<ResultHtmlDoc> SaveHtmlContent(string content, clsOntologyItem oItemHtmlDocument, clsOntologyItem oItemRef)
        {
            var taskResult = await Task.Run<ResultHtmlDoc>(() =>
            {
                var result = serviceElastic.SaveHtmlContent(oItemHtmlDocument, oItemRef, content);
                return result;
            });

            return taskResult;
        }

        public async Task<ResultTypedTags> SaveTags(clsOntologyItem oItemHtml, List<TypedTag> typedTags)
        {
            var typedTaggingConnector = new TypedTaggingModule.Connectors.TypedTaggingConnector(Globals);
            var resultTask = await typedTaggingConnector.SaveTypedTags(oItemHtml, typedTags, true);
            var result = new ResultTypedTags
            {
                Result = resultTask.Result,
                TaggingSource = resultTask.Result,
                Tags = resultTask.Tags,
                TypedTags = resultTask.TypedTags
            };
            ResultTypedTags = result;
            return result;
        }

        public async Task<ResultTypedTags> SaveTags(clsOntologyItem oItemHtml, List<clsOntologyItem> oItems, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {

            var typedTaggingConnector = new TypedTaggingModule.Connectors.TypedTaggingConnector(Globals);
            var resultTask = await typedTaggingConnector.SaveTags(oItemHtml, oItems, oItemUser, oItemGroup, true, null);
            var result = new ResultTypedTags
            {
                Result = resultTask.Result,
                TaggingSource = resultTask.Result,
                Tags = resultTask.Tags,
                TypedTags = resultTask.TypedTags
            };

            if (result.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result.Additional1 = "Error while saving typed tags, related to Html-Object";
                return result;
            }



            ResultTypedTags = result;
            return result;


        }

        public async Task<clsOntologyItem> DeleteHistoryItems(List<string> historyItemIds)
        {
            var result = Globals.LState_Success.Clone();

            var serviceResult = await serviceElastic.DeleteHistoryItems(historyItemIds.ToList());

            return serviceResult;
        }

        public async Task<ResultItem<List<StatWorkerItem>>> SyncHtmlState(SyncHtmlStateRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<StatWorkerItem>>>(async() =>
            {
                var result = new ResultItem<List<StatWorkerItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<StatWorkerItem>()
                };

                var elasticAgent = new HtmlEditAgent_Elastic(Globals);

                if (request.Start == null)
                {
                    request.MessageOutput?.OutputInfo("Get last sync datetime");
                    var lastEndResult = await elasticAgent.GetLastHtmlStatLog();

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.Start = lastEndResult.Result;
                }
                

                var lastDateString = request.Start?.ToString() ?? "-";

                request.MessageOutput?.OutputInfo($"Have last sync datetime:{lastDateString}");

                var getHtmlHistoryEntriesRequest = new GetHtmlHistoriesByDateRangeRequest();

                if (request.Start == null)
                {
                    getHtmlHistoryEntriesRequest.End = DateTime.Now.Date.Subtract(new TimeSpan(360, 0, 0, 0)).Subtract(new TimeSpan(1));
                }
                else
                {
                    getHtmlHistoryEntriesRequest.Start = request.Start.Value;
                    var days = 30;
                    var daysToNow = (DateTime.Now - getHtmlHistoryEntriesRequest.Start.Value).Days;
                    
                    if (daysToNow < 30)
                    {
                        days = daysToNow - 1;
                    }

                    if (days > 0)
                    {
                        getHtmlHistoryEntriesRequest.End = getHtmlHistoryEntriesRequest.Start.Value.AddDays(days + 1).Subtract(new TimeSpan(1));
                    }
                    else
                    {
                        var end = DateTime.Now.Subtract(new TimeSpan(0, 0, 10));
                        if (end.Subtract(getHtmlHistoryEntriesRequest.Start.Value).TotalSeconds > 60)
                        {
                            getHtmlHistoryEntriesRequest.End = DateTime.Now.Subtract(new TimeSpan(0, 0, 10));
                        }
                    }
                }

                if (getHtmlHistoryEntriesRequest.End == null)
                {
                    result.ResultState = Globals.LState_Nothing.Clone();
                    return result;
                }

                var fromString = getHtmlHistoryEntriesRequest.Start.HasValue ? getHtmlHistoryEntriesRequest.Start.Value.ToString() : "x";
                var toString = getHtmlHistoryEntriesRequest.End.HasValue ? getHtmlHistoryEntriesRequest.End.Value.ToString() : "x";
                request.MessageOutput?.OutputInfo($"Request entries from {fromString} to {toString}...");

                var getHtmlHistoryByDateEntriesResult = await GetHtmlHistoryByDateEntries(getHtmlHistoryEntriesRequest);

                result.ResultState = getHtmlHistoryByDateEntriesResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have {getHtmlHistoryByDateEntriesResult.Result.HtmlHistoryEntries.Count} entries.");

                request.MessageOutput?.OutputInfo($"Get daystats...");
                var dayStatsResult = await GetHtmlHistoryDayStat(getHtmlHistoryByDateEntriesResult.Result.HtmlHistoryEntries);

                result.ResultState = dayStatsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have {dayStatsResult.Result.Count} daystats.");


                request.MessageOutput?.OutputInfo($"Save State-Worker-Item...");
                DateTime start = DateTime.Now;

                if (getHtmlHistoryEntriesRequest.Start == null)
                {
                    var firstEntry = dayStatsResult.Result.Select(stat => stat.FirstEdit).OrderBy(firstEdit => firstEdit).FirstOrDefault();
                    if (firstEntry == null)
                    {
                        result.ResultState = Globals.LState_Nothing.Clone();
                        return result;
                    }
                    start = firstEntry.Value;
                }
                else
                {
                    start = getHtmlHistoryEntriesRequest.Start.Value;
                }

                var statWorker = new StatWorkerItem
                {
                    Start = start,
                    Ende = getHtmlHistoryEntriesRequest.End.Value,
                    DayStats = dayStatsResult.Result
                };

                var saveResult = await elasticAgent.SaveDayStats(statWorker);

                result.ResultState = saveResult;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Saved State-Worker-Item: GUID:{statWorker.IdStatWorker}, Name:{statWorker.NameStatWorker}.");

                result.Result.Add(statWorker);

                var nextStatWorkerRequest = new SyncHtmlStateRequest
                {
                    Start = getHtmlHistoryEntriesRequest.End.Value.AddTicks(1)
                };

                var nextStatWorkerResult = await SyncHtmlState(nextStatWorkerRequest);

                result.ResultState = nextStatWorkerResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.AddRange(nextStatWorkerResult.Result);

                return result;
            });

            return taskResult;
                
        }

        public async Task<ResultItem<List<ReferencesDayStat>>> GetHtmlHistoryDayStat(List<HtmlHistoryItem> historyEntries)
        {
            var taskResult = await Task.Run<ResultItem<List<ReferencesDayStat>>>(() =>
           {
               var result = new ResultItem<List<ReferencesDayStat>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<ReferencesDayStat>()
               };

               result.Result = historyEntries.
                               GroupBy(historyEntry => new { historyEntry.IdRef, historyEntry.NameRef, historyEntry.IdRefParent, historyEntry.TypeParent, Day = historyEntry.HistoryStamp.Date }).
                               Select(grp => new ReferencesDayStat 
                               { 
                                   Day = grp.Key.Day, 
                                   IdRefItem = grp.Key.IdRef, 
                                   NameRefItem = grp.Key.NameRef, 
                                   IdRefParentItem = grp.Key.IdRefParent,
                                   TypeRefItem = grp.Key.TypeParent,
                                   FirstEdit = grp.Min(itm => itm.HistoryStamp), 
                                   LastEdit = grp.Max(itm => itm.HistoryStamp),
                                   SaveCount = grp.Count()
                               }).ToList();
                               
               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetHtmlHistoryByDateEntriesResult>> GetHtmlHistoryByDateEntries(GetHtmlHistoriesByDateRangeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetHtmlHistoryByDateEntriesResult>>(async() =>
           {
               var result = new ResultItem<GetHtmlHistoryByDateEntriesResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetHtmlHistoryByDateEntriesResult()
               };

               var serviceAgent = new HtmlEditAgent_Elastic(Globals);

               var start = request.Start?.ToString() ?? "x";
               var end = request.End?.ToString() ?? "x";

               request.MessageOutput?.OutputInfo($"Getting the raw entries for start ({start} - {end})...");

               var getHtmlHistoryEntries = await serviceAgent.GetHtmlHistoryEntries(request);

               result.ResultState = getHtmlHistoryEntries.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Having the raw entries: {getHtmlHistoryEntries.Result.HtmlHistoryEntries.Count}");


               request.MessageOutput?.OutputInfo($"Getting the entries...");
               var factory = new Factories.HtmlHistoryFactory();
               var factoryResult = await factory.GetHistoryEntries(getHtmlHistoryEntries.Result, Globals);

               result.ResultState = factoryResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Having {factoryResult.Result.Count} entries.");

               result.Result.HtmlHistoryEntries = factoryResult.Result;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<HtmlHistoryItemWithRaw>>> GetHtmlHistory(clsOntologyItem oItemHtml, clsOntologyItem refItem)
        {
            var result = new ResultItem<List<HtmlHistoryItemWithRaw>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<HtmlHistoryItemWithRaw>()
            };

            var serviceResult = await serviceElastic.GetHtmlHistoryEntries(oItemHtml);

            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                return result;
            }

            var factory = new HtmlHistoryFactory();

            result = await factory.GetHistoryEntries(oItemHtml, refItem, serviceResult.Result, Globals);



            return result;
        }

        public async Task<ResultItem<List<HtmlSearchResultItem>>> GetHtmlSearchResult(string searchString, IMessageOutput messageOutput = null)
        {
            var result = new ResultItem<List<HtmlSearchResultItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<HtmlSearchResultItem>()
            };

            messageOutput?.OutputInfo("Search for string in HTML-Pages...");
            var serviceResult = await serviceElastic.GetHtmlSearchResult(searchString);

            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                messageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }
            messageOutput?.OutputInfo($"Found {serviceResult.Result.FoundHtmlCode.Count} Html-Texts.");

            var factory = new HtmlSearchResultFactory(Globals);

            messageOutput?.OutputInfo("Create search-result...");
            result = await factory.CreateHtmlSearchResultList(serviceResult.Result, searchString);

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                messageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            messageOutput?.OutputInfo("Created search-result.");

            return result;
        }

        public async Task<ResultItem<HTMLDiffResult>> DiffHtml(HTMLDiffRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<HTMLDiffResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new HTMLDiffResult()
                };

                request.MessageOutput?.OutputInfo("Validate Request...");
                result.ResultState = ValidationController.ValidateHTMLDiffRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated Request.");

                
                foreach (var htmlDiffItem in request.HtmlDiffItems)
                {
                    var htmlDiff = new HtmlDiff.HtmlDiff(htmlDiffItem.Html1, htmlDiffItem.Html2);
                    htmlDiffItem.HtmlDiff = htmlDiff.Build();
                    result.Result.HTMLDiffItems.Add(new ResultItem<HTMLDiffItem> { ResultState = Globals.LState_Success.Clone(), Result = htmlDiffItem });
                }

                return result;
            });
            return taskResult;
        }

        private void Initialize()
        {
            serviceElastic = new HtmlEditAgent_Elastic(Globals);
            typedTaggingConnector = new TypedTaggingConnector(Globals);
        }
    }

    public class ResultGetHtmlHierarchy
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem HtmlDocumentOfRef { get; set; }
        public List<clsObjectRel> Hierarchy { get; set; }
    }

}
