﻿using OntologyClasses.BaseClasses;

namespace HtmlEditorModule
{
    public class ResultHtmlObject
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem OItemHtmlItem { get; set; }
    }
}
